//
//  MainScene.m
//  NovaFusion
//
//  Created by tiseno on 9/28/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#define SectionTableHeight 118
#define ViewHeight 718
#define BookmarkTableHeight 105
#define BookmarkTableWidth 300
#define MyJobTableWidth 300
#define InfoTableHeight 140
#define InfoTableWidth 300
#define HelpTableHeight 70
#define HelpTableWidth 600

#import "MainScene.h"

@implementation MainScene

@synthesize sectiontable, imgPicker, searchbar, subview, mainArr;

@synthesize myjobview, myjobtable, jobtitletextfield, myjobtitlelabel;

@synthesize bookmarkview, bookmarktable, editbookmarkbutton, donebookmarkbutton, emailbookmarkbutton, bookmarktitlelabel;

@synthesize infoview, infotable, joblabel, totalcostview, tradelabel, retaillabel, editjobview, toolbar, editjobinfobutton, donejobinfobutton, emailjobinfobutton, infotitlelabel;

@synthesize achview, titlelabel, descriptionlabel, description2label;

@synthesize helpview, helptable, backgroundimage;

@synthesize emailcontroller, sendcontroller;

@synthesize itemCodeArr, itemImageArr, itemNameArr, itemPriceArr, itemAppIdArr, itemIdArr, jobTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.sectiontable setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    
    self.searchbar.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.searchbar.layer.borderWidth = 1;
    
    UIView *headerview = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.height, 25)] autorelease];
    self.sectiontable.tableHeaderView = headerview;
    self.sectiontable.tableFooterView = [[[UIView alloc] init] autorelease];
    self.bookmarktable.tableFooterView = [[[UIView alloc] init] autorelease];
    self.myjobtable.tableFooterView = [[[UIView alloc] init] autorelease];
    self.helptable.tableFooterView = [[[UIView alloc] init] autorelease];
    self.infotable.tableFooterView = [[[UIView alloc] init] autorelease];
    
    gesturetapview = [[UIView alloc]init];
    gesturetapview.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singletapcaptured:)];
    [gesturetapview addGestureRecognizer:singletap];
    [gesturetapview setUserInteractionEnabled:YES];
    
    db = [[DatabaseAction alloc] init];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    jobtitletextfield.enablesReturnKeyAutomatically = NO;
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [gesturetapview release];
    
    [imgPicker release];
    [sectiontable release];
    [searchbar release];
    [subview release];
    
    [myjobview release];
    [myjobtable release];
    [jobtitletextfield release];
    [myjobtitlelabel release];
    
    [bookmarkview release];
    [bookmarktable release];
    [editbookmarkbutton release];
    [donebookmarkbutton release];
    [emailbookmarkbutton release];
    [bookmarktitlelabel release];
    
    [infoview release];
    [infotable release];
    [joblabel release];
    [totalcostview release];
    [tradelabel release];
    [retaillabel release];
    [editjobview release];
    [toolbar release];
    [editjobinfobutton release];
    [donejobinfobutton release];
    [emailjobinfobutton release];
    [infotitlelabel release];
    
    [achview release];
    [titlelabel release];
    [descriptionlabel release];
    [description2label release];
    
    [sendcontroller release];
    [emailcontroller release];
    
    [db release];
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(tableView == self.sectiontable)
        return mainArr.count;
    else if(tableView == self.myjobtable)
        return joblist.count;
    else if(tableView == self.bookmarktable)
    {
        return bookmarklist.count;
    }
    else if(tableView == self.infotable)
        return productlist.count;
    else
        return 14;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.sectiontable)
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        sectioncell = (SectionCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(sectioncell == nil){
            sectioncell = [[[SectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        NSError* error;
        NSDictionary* novainfo = [self.mainArr objectAtIndex:indexPath.row];
        NSDictionary* novadetails = [novainfo objectForKey:@"application"];
        
        sectioncell.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.height, SectionTableHeight);
        sectioncell.selectionStyle = UITableViewCellSelectionStyleNone;
        sectioncell.title.text = [novadetails objectForKey:@"name"];
        
        //NSLog(@"%@", [novadetails objectForKey:@"id"]);
        
        NSDictionary *details = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"params"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        sectioncell.description.text = [details objectForKey:@"content.teaser_description"];
        
        sectioncell.sectionimageview.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [details objectForKey:@"content.teaser_image"]]];
        
        [sectioncell setBackgroundView:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"category_%d.png", indexPath.row + 1]]] autorelease]];
        return sectioncell;
    }
    else if(tableView == self.myjobtable)
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        myjobcell = (MyJobCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(myjobcell == nil){
            myjobcell = [[[MyJobCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        myjobcell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        joblist = [db retrieveMyjob];
        Myjob *myjob = [joblist objectAtIndex:indexPath.row];
        myjobcell.myjobtitle.text = [myjob.myjobtitle stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];;
        myjobcell.myjobdate.text = myjob.myjobdate;
        
        CGSize maxsize = CGSizeMake(200, 9999);
        UIFont *thefont = [UIFont boldSystemFontOfSize:16];
        CGSize textsize = [myjobcell.myjobtitle.text sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        myjobcell.myjobtitle.frame = CGRectMake(20, 5, 200, textsize.height);
        myjobcell.myjobdate.frame = CGRectMake(20, myjobcell.myjobtitle.frame.origin.y + myjobcell.myjobtitle.frame.size.height + 5, 200, 15);
        myjobcell.frame = CGRectMake(0, 0, MyJobTableWidth, 40 + textsize.height);
        CGRect imageframe = CGRectMake(270, (myjobcell.frame.size.height - myjobcell.arrowimage.frame.size.height) / 2, myjobcell.arrowimage.frame.size.width, myjobcell.arrowimage.frame.size.height);
        myjobcell.arrowimage.frame = imageframe;
        
        return myjobcell;
    }
    else if(tableView == self.bookmarktable)
    {
        decider = 1;
        
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        bookmarkcell = (BookmarkCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(bookmarkcell == nil){
            bookmarkcell = [[[BookmarkCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        bookmarkcell.frame = CGRectMake(0, 0, BookmarkTableWidth, BookmarkTableHeight);
        bookmarkcell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        bookmarklist = [db retrieveBookmark];
        Bookmark *bookmark = [bookmarklist objectAtIndex:indexPath.row];
        
        bookmarkcell.titlelabel.text = bookmark.bookmarktitle;
        bookmarkcell.codelabel.text = [NSString stringWithFormat:@"(%@)", bookmark.bookmarkcode];
        bookmarkcell.productimage.imageURL = [NSURL URLWithString:bookmark.bookmarkimagepathS];
        
        UIButton *viewbutton = [[[UIButton alloc] initWithFrame:CGRectMake(236, 75, 49, 18)] autorelease];
        viewbutton.tag = indexPath.row;
        [viewbutton setBackgroundImage:[UIImage imageNamed:@"btn_view.png"] forState:UIControlStateNormal];
        [bookmarkcell addSubview:viewbutton];
        
        [viewbutton addTarget:self action:@selector(productdetailview:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if(iseditbookmark)
        {
            bookmarkcell.productimage.frame = CGRectMake(30, 0, 83, 104);
            bookmarkcell.titlelabel.frame = CGRectMake(120, 10, 170, 40);
            bookmarkcell.codelabel.frame = CGRectMake(120, 50, 170, 20);
            viewbutton.hidden = YES;
            
            UIButton *deletebutton = [[[UIButton alloc] initWithFrame:CGRectMake(5 , (BookmarkTableHeight - 27) / 2 , 27, 27)] autorelease];
            deletebutton.tag = indexPath.row;
            [deletebutton setBackgroundImage:[UIImage imageNamed:@"btn_delete.png"] forState:UIControlStateNormal];
            [bookmarkcell addSubview:deletebutton];
            
            [deletebutton addTarget:self action:@selector(deletebookmark:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            bookmarkcell.productimage.frame = CGRectMake(0, 0, 83, 104);
            bookmarkcell.titlelabel.frame = CGRectMake(90, 10, 200, 40);
            bookmarkcell.codelabel.frame = CGRectMake(90, 50, 200, 20);
            viewbutton.hidden = NO;
        }
        
        return bookmarkcell;
    }
    else if(tableView == self.infotable)
    {
        decider = 2;
        
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        infocell = (InfoCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(infocell == nil){
            infocell = [[[InfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        infocell.frame = CGRectMake(0, 0, InfoTableWidth, InfoTableHeight);
        infocell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
        productlist = [db retrieveProduct:myjobid];
        Product *product = [productlist objectAtIndex:indexPath.row];
        
        infocell.titlelabel.text = product.producttitle;
        infocell.codelabel.text = [NSString stringWithFormat:@"CODE: %@", product.productcode];
        infocell.tradelabel.text = [NSString stringWithFormat:@"TRADE: %@", product.producttrade];
        infocell.retaillabel.text = [NSString stringWithFormat:@"RETAIL: %@", product.productretail];
        infocell.quantitylabel.text = [NSString stringWithFormat:@"x %d", product.productquantity];
        infocell.productimage.imageURL = [NSURL URLWithString:product.productimagepathS];
        
        UIButton *viewbutton = [[[UIButton alloc] initWithFrame:CGRectMake(236, 115, 49, 18)] autorelease];
        viewbutton.tag = indexPath.row;
        [viewbutton setBackgroundImage:[UIImage imageNamed:@"btn_view.png"] forState:UIControlStateNormal];
        [infocell addSubview:viewbutton];
        
        [viewbutton addTarget:self action:@selector(productdetailview:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *deletebutton = [[[UIButton alloc] initWithFrame:CGRectMake(5 , (InfoTableHeight - 27) / 2 , 27, 27)] autorelease];
        int tag = [db retrieveProductselectedrow:indexPath.row :myjobid];
        deletebutton.tag = tag;
        
        if(iseditmyjob)
        {
            infocell.productimage.frame = CGRectMake(30, 0, 83, 104);
            infocell.titlelabel.frame = CGRectMake(120, 10, 170, 40);
            infocell.codelabel.frame = CGRectMake(120, 60, 170, 15);
            infocell.tradelabel.frame = CGRectMake(120, 75, 170, 15);
            infocell.retaillabel.frame = CGRectMake(120, 90, 170, 15);
            viewbutton.hidden = YES;
            
            [deletebutton setBackgroundImage:[UIImage imageNamed:@"btn_delete.png"] forState:UIControlStateNormal];
            [infocell addSubview:deletebutton];
            [deletebutton addTarget:self action:@selector(deletejobinfo:) forControlEvents:UIControlEventTouchUpInside];
            
            self.infotable.frame = CGRectMake(0, self.editjobview.frame.size.height + 44, InfoTableWidth, [[UIScreen mainScreen] bounds].size.width - self.editjobview.frame.size.height - 108);
        }
        else
        {
            infocell.productimage.frame = CGRectMake(0, 0, 83, 104);
            infocell.titlelabel.frame = CGRectMake(90, 10, 200, 40);
            infocell.codelabel.frame = CGRectMake(90, 60, 200, 15);
            infocell.tradelabel.frame = CGRectMake(90, 75, 200, 15);
            infocell.retaillabel.frame = CGRectMake(90, 90, 200, 15);
            viewbutton.hidden = NO;
            [deletebutton removeFromSuperview];
            
            self.infotable.frame = CGRectMake(0, 74, InfoTableWidth, [[UIScreen mainScreen] bounds].size.width - 138);
        }
        
        return infocell;
    }
    else
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        helpcell = (HelpCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(helpcell == nil){
            helpcell = [[[HelpCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        helpcell.frame = CGRectMake(0, 0, HelpTableWidth, HelpTableHeight);
        helpcell.selectionStyle = UITableViewCellSelectionStyleNone;
        helpcell.helpdescription.text = [appDelegate.helpdescription objectAtIndex:indexPath.row];
        helpcell.imageview.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_help_%d.png", indexPath.row + 1]];
        
        [helptable setSeparatorColor:[UIColor clearColor]];
        return helpcell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.sectiontable)
    {
        [GlobalFunction AddLoadingScreen:self];
        
        sectioncell = (SectionCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        row = indexPath.row;
        [self performSelector:@selector(delay) withObject:nil afterDelay:0.1];
    }
    else if(tableView == self.myjobtable)
    {
        self.infoview.hidden = YES;
        
        [GlobalFunction AddLoadingScreen:self];
        
        myjobcell = (MyJobCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        
        jobrowselected = indexPath.row;
        
        self.joblabel.text = myjobcell.myjobtitle.text;
        
        jobTitle = [self.joblabel.text retain];
        
        [self performSelector:@selector(delay2) withObject:nil afterDelay:0.1];
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    if(tableView == self.sectiontable)
    {
        sectioncell = (SectionCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return sectioncell.frame.size.height;
    }
    else if(tableView == self.myjobtable)
    {
        myjobcell = (MyJobCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return myjobcell.frame.size.height;
    }
    else if(tableView == self.bookmarktable)
    {
        bookmarkcell = (BookmarkCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return bookmarkcell.frame.size.height;
    }
    else if(tableView == self.infotable)
    {
        infocell = (InfoCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return infocell.frame.size.height;
    }
    else
    {
        helpcell = (HelpCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return helpcell.frame.size.height;
    }
}

-(void)delay
{
    NSDictionary* novainfo = [self.mainArr objectAtIndex:row];
    NSDictionary* novadetails = [novainfo objectForKey:@"application"];
    
    WebServices *ws = [[WebServices alloc] init];
    
    NSArray *test = [ws selectAllFromCategoryTableExceptParam:[[novadetails objectForKey:@"id"] intValue]];
    
    [ws release];
    
    [GlobalFunction RemoveLoadingScreen:self];
    ChildScene *GtestClasssViewController=[[[ChildScene alloc] initWithNibName:@"ChildScene"  bundle:nil] autorelease];
    GtestClasssViewController.categoryArr = [test mutableCopy];
    GtestClasssViewController.categorytext = [novadetails objectForKey:@"name"];
    GtestClasssViewController.row = row;
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(void)delay2
{
    [GlobalFunction RemoveLoadingScreen:self];
    gesturetapview.frame = CGRectMake(600, 0, 424, 768);
    int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
    productlist = [db retrieveProduct:myjobid];
    self.infotitlelabel.textColor = [UIColor colorWithRed:112.0/255.0 green:119.0/255.0 blue:127.0/255.0 alpha:1.0];
    self.editjobview.hidden = YES;
    self.infoview.hidden = NO;
    self.editjobinfobutton.enabled = YES;
    self.donejobinfobutton.enabled = NO;
    iseditmyjob = NO;
    if(productlist.count > 0)
    {
        totalretail = 0.00;
        totaltrade = 0.00;
        self.infotable.tableFooterView = self.totalcostview;
        self.totalcostview.backgroundColor = [UIColor colorWithRed:14.0/255.0 green:23.0/255.0 blue:32.0/255.0 alpha:1.0];
        for(int i = 0; i < productlist.count;i++)
        {
            Product *product = [productlist objectAtIndex:i];
            
            if(![product.producttrade isEqualToString:@"-"])
            {
                NSString *trade = [product.producttrade substringWithRange:NSMakeRange(1, product.producttrade.length - 10)];
                trade = [trade stringByReplacingOccurrencesOfString:@"," withString:@""];
                
                totaltrade += ([trade floatValue] * product.productquantity);
            }
            self.tradelabel.text = [NSString stringWithFormat:@"%.2f", totaltrade];
            
            if(![product.productretail isEqualToString:@"-"])
            {
                NSString *retail = [product.productretail substringWithRange:NSMakeRange(1, product.productretail.length - 10)];
                retail = [retail stringByReplacingOccurrencesOfString:@"," withString:@""];
                
                totalretail += ([retail floatValue] * product.productquantity);
            }
            self.retaillabel.text = [NSString stringWithFormat:@"%.2f", totalretail];
        }
    }
    else
    {
        self.infotable.tableFooterView = [[[UIView alloc] init] autorelease];
    }
    [self.infotable reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar
{
    [GlobalFunction AddLoadingScreen:self];
    [self performSelector:@selector(delay3) withObject:nil afterDelay:0.1];
    [self.searchbar resignFirstResponder];
}

-(void)delay3
{
    NSError *error;
    
    WebServices *ws = [[WebServices alloc] init];
    
    NSArray *result = [ws searchItemByKeywords:self.searchbar.text];
    
    if(result.count > 0)
    {
        itemCodeArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemImageArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemNameArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemAppIdArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemIdArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        
        for(int i = 0; i < result.count; i++)
        {
            NSDictionary* novainfo = [result objectAtIndex:i];
            
            NSDictionary* novadetails = [novainfo objectForKey:@"item"];
            
            [itemNameArr addObject:[novadetails objectForKey:@"name"]];
            
            NSDictionary *elementDetails = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
            
            //Print the price of the product
            NSDictionary *priceDetails = [elementDetails objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
            NSDictionary *price = [priceDetails objectForKey:@"0"];
            
            [itemPriceArr addObject:[price objectForKey:@"value"]];
            
            NSDictionary *itemDic = [elementDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
            NSDictionary *itemCode = [itemDic objectForKey:@"0"];
            
            [itemCodeArr addObject:[itemCode objectForKey:@"value"]];
            
            NSDictionary *thumbpath = [elementDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
            
            [itemImageArr addObject:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
            
            [itemAppIdArr addObject:[novadetails objectForKey:@"application_id"]];
            
            [itemIdArr addObject:[novadetails objectForKey:@"id"]];
        }
    }
    
    [ws release];
    
    [GlobalFunction RemoveLoadingScreen:self];
    SearchScene *GtestClasssViewController=[[[SearchScene alloc] initWithNibName:@"SearchScene"  bundle:nil] autorelease];
    
    GtestClasssViewController.keyword = self.searchbar.text;
    GtestClasssViewController.itemCodeArr = itemCodeArr;
    GtestClasssViewController.itemImageArr = itemImageArr;
    GtestClasssViewController.itemNameArr = itemNameArr;
    GtestClasssViewController.itemPriceArr = itemPriceArr;
    GtestClasssViewController.itemAppIdArr = itemAppIdArr;
    GtestClasssViewController.itemIdArr = itemIdArr;
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
    self.searchbar.text = @"";
}

-(void)delay4
{
    NSError *error;
    
    WebServices *ws = [[WebServices alloc] init];
    NSArray *result;
    result = [ws searchItemByBarCode:data];
    
    if(result.count > 0)
    {
        itemCodeArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemImageArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemNameArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemAppIdArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemIdArr = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        
        for(int i = 0; i < result.count; i++)
        {
            NSDictionary* novainfo = [result objectAtIndex:i];
            
            NSDictionary* novadetails = [novainfo objectForKey:@"item"];
            
            [itemNameArr addObject:[novadetails objectForKey:@"name"]];
            
            NSDictionary *elementDetails = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
            
            //Print the price of the product
            NSDictionary *priceDetails = [elementDetails objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
            NSDictionary *price = [priceDetails objectForKey:@"0"];

            [itemPriceArr addObject:[price objectForKey:@"value"]];
            
            NSDictionary *itemDic = [elementDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
            NSDictionary *itemCode = [itemDic objectForKey:@"0"];
            
            [itemCodeArr addObject:[itemCode objectForKey:@"value"]];
            
            NSDictionary *thumbpath = [elementDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
            
            [itemImageArr addObject:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
            
            [itemAppIdArr addObject:[novadetails objectForKey:@"application_id"]];
            
            [itemIdArr addObject:[novadetails objectForKey:@"id"]];
        }
    }
    
    [ws release];
    
    [GlobalFunction RemoveLoadingScreen:self];
    SearchScene *GtestClasssViewController=[[[SearchScene alloc] initWithNibName:@"SearchScene"  bundle:nil] autorelease];
    
    GtestClasssViewController.keyword = self.searchbar.text;
    GtestClasssViewController.itemCodeArr = itemCodeArr;
    GtestClasssViewController.itemImageArr = itemImageArr;
    GtestClasssViewController.itemNameArr = itemNameArr;
    GtestClasssViewController.itemPriceArr = itemPriceArr;
    GtestClasssViewController.itemAppIdArr = itemAppIdArr;
    GtestClasssViewController.itemIdArr = itemIdArr;
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)about:(id)sender
{
    [self.contactusview removeFromSuperview];
    [self.helpview removeFromSuperview];
    [self.achview setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    self.achview.frame = CGRectMake(0, 30, [[UIScreen mainScreen] bounds].size.height, ViewHeight);
    [self.view addSubview:self.achview];
}

-(IBAction)contactus:(id)sender
{
    [self.achview removeFromSuperview];
    [self.helpview removeFromSuperview];
    self.contactdescriptionlabel.text = @"Call us today on\n- 1300 CRH AUS that's 1300 274 287 /\n- 0011+61+ 3 9532 1599 (International Phone Line)\n\n\nLines are open 8am to 5pm.\nFax: 1300 CRH FAX or 1300 274 329";
    [self.contactusview setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    self.contactusview.frame = CGRectMake(0, 30, [[UIScreen mainScreen] bounds].size.height, ViewHeight);
    [self.view addSubview:self.contactusview];
}

-(IBAction)help:(id)sender
{
    [self.achview removeFromSuperview];
    [self.contactusview removeFromSuperview];
    self.helpview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    self.helpview.frame = CGRectMake(0, 30, [[UIScreen mainScreen] bounds].size.height, ViewHeight);
    [self.view addSubview:self.helpview];
}

-(IBAction)close:(id)sender
{
    [self.achview removeFromSuperview];
}

-(IBAction)closehelp:(id)sender
{
    [self.helpview removeFromSuperview];
}

-(IBAction)closecontactus:(id)sender
{
    [self.contactusview removeFromSuperview];
}

//contact us//
-(IBAction)emailcontactus:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:@""];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"info@crh.com.au", nil];
        [mailer setToRecipients:toRecipients];
        
        // only for iPad
        mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentModalViewController:mailer animated:YES];
        
        [mailer release];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

-(IBAction)facebookcontactus:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://www.facebook.com/crh.australia" ];
    [[UIApplication sharedApplication] openURL:url];
}

-(IBAction)linkedcontactus:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://au.linkedin.com/pub/crh-australia/5a/b14/2a1" ];
    [[UIApplication sharedApplication] openURL:url];
}
//////////////

-(IBAction)myjobs:(id)sender{
    [self.subview setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    self.myjobtitlelabel.textColor = [UIColor colorWithRed:112.0/255.0 green:119.0/255.0 blue:127.0/255.0 alpha:1.0];
    self.myjobview.hidden = NO;
    self.bookmarkview.hidden = YES;
    self.infoview.hidden = YES;
    joblist = [db retrieveMyjob];
    [self.myjobtable reloadData];
    gesturetapview.frame = CGRectMake(300, 0, 724, 768);
    [self.subview addSubview:gesturetapview];
    [self.view addSubview:self.subview];
}

-(IBAction)bookmarks:(id)sender
{
    [self.subview setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    self.bookmarktitlelabel.textColor = [UIColor colorWithRed:112.0/255.0 green:119.0/255.0 blue:127.0/255.0 alpha:1.0];
    self.bookmarkview.hidden = NO;
    self.myjobview.hidden = YES;
    self.infoview.hidden = YES;
    bookmarklist = [db retrieveBookmark];
    [self.bookmarktable reloadData];
    iseditbookmark = NO;
    self.donebookmarkbutton.enabled = NO;
    self.editbookmarkbutton.enabled = YES;
    isbookmark = YES;
    gesturetapview.frame = CGRectMake(0, 0, 724, 768);
    [self.subview addSubview:gesturetapview];
    [self.view addSubview:self.subview];
}

-(IBAction)scan:(id)sender
{
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    
    reader.readerView.torchMode = 0;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25 config: ZBAR_CFG_ENABLE to: 0];
    
    // present and release the controller
    [self presentModalViewController: reader
                            animated: YES];
    [reader release];
}

-(IBAction)launchsafari:(id)sender
{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://www.crh.com.au" ];
    [[UIApplication sharedApplication] openURL:url];
}

- (void) readerControllerDidFailToRead: (ZBarReaderController*) reader withRetry: (BOOL) retry
{
    NSLog(@"the image picker failing to read");
    
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    NSString *hiddenData;
    for(symbol in results)
        hiddenData=[NSString stringWithString:symbol.data];
    
    NSLog(@"BARCODE= %@",symbol.data);
    
    NSUserDefaults *storeData=[NSUserDefaults standardUserDefaults];
    [storeData setObject:hiddenData forKey:@"CONSUMERID"];
    NSLog(@"SYMBOL : %@",hiddenData);
    
    [reader dismissModalViewControllerAnimated: NO];
    
    data = symbol.data;
    if(data != nil )
    {
        [GlobalFunction AddLoadingScreen:self];
        [self performSelector:@selector(delay4) withObject:nil afterDelay:0.1];
    }
    else
    {
        data = hiddenData;
        if([data rangeOfString:@"http"].location != NSNotFound)
        {
            NSURL *url = [ [ NSURL alloc ] initWithString:data];
            [[UIApplication sharedApplication] openURL:url];
        }
        else
        {
            [GlobalFunction AddLoadingScreen:self];
            [self performSelector:@selector(delay4) withObject:nil afterDelay:0.1];
        }
    }
}

//subview gesture//
-(void) singletapcaptured:(UIGestureRecognizer *)gesture
{
    [gesturetapview removeFromSuperview];
    [self.subview removeFromSuperview];
}
///////////////////

//myjob//
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [jobtitletextfield resignFirstResponder];
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

-(IBAction)addnewjob:(id)sender
{
    self.infoview.hidden = YES;
    gesturetapview.frame = CGRectMake(300, 0, 724, 768);
    alert = [[UIAlertView alloc] initWithTitle:@"\n" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
    newjobtitletextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 15.0, 260.0, 25.0)] autorelease];
    newjobtitletextfield.delegate = self;
    [newjobtitletextfield becomeFirstResponder];
    [newjobtitletextfield setBackgroundColor:[UIColor whiteColor]];
    newjobtitletextfield.textAlignment=UITextAlignmentCenter;
    newjobtitletextfield.layer.cornerRadius=5.0;
    [alert addSubview:newjobtitletextfield];
    [alert show];
    [alert release];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0){
        [alert dismissWithClickedButtonIndex:0 animated:NO];
    }else{
        NSString *check = [newjobtitletextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if(![check isEqualToString:@""])
        {
            if([check length] > 32)
            {
                [self.view makeToast:@"Job name is too, please shorten it!" duration:(0.3) position:@"center"];
                [newjobtitletextfield becomeFirstResponder];
                [alert show];
            }
            else
            {
                [alert dismissWithClickedButtonIndex:1 animated:NO];
                [self.view makeToast:@"Successfully created new job！" duration:(0.3) position:@"center"];
                NSDate *today = [NSDate date];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                dateFormat.locale = [NSLocale currentLocale];
                [dateFormat setDateFormat:@"EEEE, dd MMMM YYYY"];
                NSString *dateString = [dateFormat stringFromDate:today];
                [dateFormat release];
                
                Myjob *myjob = [[Myjob alloc] init];
                myjob.myjobtitle = [newjobtitletextfield.text stringByReplacingOccurrencesOfString:@"'" withString:@"\""];;
                myjob.myjobdate = dateString;
                [db insertMyjob:myjob];
                [myjob release];
                
                joblist = [db retrieveMyjob];
                [self.myjobtable reloadData];
            }
        }
        else
        {
            [self.view makeToast:@"Please give new job a name!" duration:(0.3) position:@"center"];
            [newjobtitletextfield becomeFirstResponder];
            [alert show];
        }
    }
}

-(IBAction)deletejob:(id)sender
{
    int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
    [db deleteMyjob:myjobid];
    [db deleteProductWithJobid:myjobid];
    joblist = [db retrieveMyjob];
    self.infoview.hidden = YES;
    self.editjobview.hidden = YES;
    [self.myjobtable reloadData];
}
////////

//bookmark//
-(IBAction)editbookmark:(id)sender
{
    iseditbookmark = !iseditbookmark;
    self.donebookmarkbutton.enabled = YES;
    self.editbookmarkbutton.enabled = NO;
    bookmarklist = [db retrieveBookmark];
    [self.bookmarktable reloadData];
}

-(IBAction)donebookmark:(id)sender
{
    self.donebookmarkbutton.enabled = NO;
    self.editbookmarkbutton.enabled = YES;
    iseditbookmark = !iseditbookmark;
    bookmarklist = [db retrieveBookmark];
    [self.bookmarktable reloadData];
}

-(IBAction)deletebookmark:(id)sender
{
    int bookmarkid = [db retrieveBookmarkselectedrow:[sender tag]];
    [db deleteBookmark:bookmarkid];
    bookmarklist = [db retrieveBookmark];
    [self.bookmarktable reloadData];
    
}
////////////

//info//
-(IBAction)editjobinfo:(id)sender
{
    self.donejobinfobutton.enabled = YES;
    self.editjobinfobutton.enabled = NO;
    iseditmyjob = YES;
    int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
    productlist = [db retrieveProduct:myjobid];
    self.editjobview.hidden = NO;
    
    self.jobtitletextfield.text = [[db retrieveSelectedmyjobtitle:myjobid] stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
    [self.infotable reloadData];
    gesturetapview.frame = CGRectMake(600, 0, 724, 768);
}

-(IBAction)donejobinfo:(id)sender
{
    self.editjobinfobutton.enabled = NO;
    NSString *check = [self.jobtitletextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(![check isEqualToString:@""])
    {
        if([check length] > 32)
        {
            [self.view makeToast:@"Job name is too, please shorten it!" duration:(0.3) position:@"center"];
        }
        else
        {
            self.donejobinfobutton.enabled = NO;
            self.editjobinfobutton.enabled = YES;
            iseditmyjob = NO;
            int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
            [db updateMyjobtitle:myjobid :[self.jobtitletextfield.text stringByReplacingOccurrencesOfString:@"'" withString:@"\""]];
            [jobtitletextfield resignFirstResponder];
            self.infoview.hidden = YES;
            joblist = [db retrieveMyjob];
            [self.myjobtable reloadData];
            gesturetapview.frame = CGRectMake(300, 0, 724, 768);
        }
    }
    else
    {
        [self.view makeToast:@"Job Name cannot be empty!" duration:(0.3) position:@"center"];
    }
}

-(IBAction)deletejobinfo:(id)sender
{
    int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
    [db deleteProduct:[sender tag] :myjobid];
    productlist = [db retrieveProduct:myjobid];
    [self.infotable reloadData];
    if(productlist.count <= 0)
    {
        self.infotable.tableFooterView = [[[UIView alloc] init] autorelease];
    }
    else
    {
        totalretail = 0.00;
        totaltrade = 0.00;
        
        for(int i = 0; i < productlist.count;i++)
        {
            Product *product = [productlist objectAtIndex:i];
            
            if(![product.producttrade isEqualToString:@"-"])
            {
                NSString *trade = [product.producttrade substringWithRange:NSMakeRange(1, product.producttrade.length - 10)];
                trade = [trade stringByReplacingOccurrencesOfString:@"," withString:@""];
                
                totaltrade += ([trade floatValue] * product.productquantity);
            }
            self.tradelabel.text = [NSString stringWithFormat:@"%.2f", totaltrade];
            
            if(![product.productretail isEqualToString:@"-"])
            {
                NSString *retail = [product.productretail substringWithRange:NSMakeRange(1, product.productretail.length - 10)];
                retail = [retail stringByReplacingOccurrencesOfString:@"," withString:@""];
                
                totalretail += ([retail floatValue] * product.productquantity);
            }
            self.retaillabel.text = [NSString stringWithFormat:@"%.2f", totalretail];
        }
    }
}
////////

//productdetailview//
-(IBAction)productdetailview:(id)sender
{
    ChildScene *GtestClasssViewController=[[[ChildScene alloc] initWithNibName:@"ChildScene"  bundle:nil] autorelease];
    if(isbookmark)
    {
        bookmarklist = [db retrieveBookmark];
        Bookmark *bookmark = [bookmarklist objectAtIndex:[sender tag]];
        
        GtestClasssViewController.appID = [NSString stringWithFormat:@"%d", bookmark.sectionid];
        GtestClasssViewController.itemID = [NSString stringWithFormat:@"%d",bookmark.categoryid];
        GtestClasssViewController.viewOption = @"1";
    }
    else
    {
        int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
        productlist = [db retrieveProduct:myjobid];
        Product *product = [productlist objectAtIndex:[sender tag]];
    
        GtestClasssViewController.appID = [NSString stringWithFormat:@"%d",product.sectionid];
        GtestClasssViewController.itemID = [NSString stringWithFormat:@"%d",product.categoryid];
        GtestClasssViewController.viewOption = @"1";
    }
    //NSLog(@"%d", bookmark.sectionid);
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
}
/////////////////////

//popupview//
-(IBAction)popup:(id)sender
{
    emailpop = [[UIPopoverController alloc] initWithContentViewController:self.emailcontroller];
    emailpop.delegate = self;
    if([sender tag] == 988)
    {
        int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
        productlist = [db retrieveProduct:myjobid];
        if(productlist.count > 0)
        {
            tagbutton = 988;
            [emailpop presentPopoverFromRect:self.emailjobinfobutton.frame inView:self.infoview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        }
    }
    else
    {
        bookmarklist = [db retrieveBookmark];
        if(bookmarklist.count > 0)
        {
            tagbutton = 101;
            [emailpop presentPopoverFromRect:self.emailbookmarkbutton.frame inView:self.bookmarkview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        }
    }
}

-(IBAction)sendwithprice:(id)sender
{
    isneedprice = YES;
    [emailpop dismissPopoverAnimated:YES];
    sendpop = [[UIPopoverController alloc] initWithContentViewController:self.sendcontroller];
    sendpop.delegate = self;
    if(tagbutton == 988)
    {
        [sendpop presentPopoverFromRect:self.emailjobinfobutton.frame inView:self.infoview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    else
        [sendpop presentPopoverFromRect:self.emailbookmarkbutton.frame inView:self.bookmarkview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

-(IBAction)sendwithoutprice:(id)sender
{
    isneedprice = NO;
    [emailpop dismissPopoverAnimated:YES];
    sendpop = [[UIPopoverController alloc] initWithContentViewController:self.sendcontroller];
    sendpop.delegate = self;
    if(tagbutton == 988)
    {
        [sendpop presentPopoverFromRect:self.emailjobinfobutton.frame inView:self.infoview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    else
        [sendpop presentPopoverFromRect:self.emailbookmarkbutton.frame inView:self.bookmarkview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

-(IBAction)sendplain:(id)sender
{
    if([GlobalFunction NetworkStatus])
    {
        if ([MFMailComposeViewController canSendMail])
        {
            NSMutableString *subject = [[NSMutableString alloc] init];
            NSMutableString *emailBody = [[NSMutableString alloc] init];
            
            if(decider == 1)
            {
                [subject appendString:@"CRH - My Bookmarks"];
                
                bookmarklist = [db retrieveBookmark];
                
                if(isneedprice)
                {
                    for(int i = 0; i < bookmarklist.count; i++)
                    {
                        Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                        
                        NSMutableString *appStr = [NSMutableString stringWithFormat:@"\n\n\n%d) %@\n\n%@\n\n%@\n\n%@\n\n%@", i+1, [NSString stringWithFormat:@"Title : %@", bookmark.bookmarktitle], [NSString stringWithFormat:@"Code : %@", bookmark.bookmarkcode], [NSString stringWithFormat:@"Trade: %@", bookmark.bookmarktrade], [NSString stringWithFormat:@"Retail: %@", bookmark.bookmarkretail], bookmark.bookmarkdescription];
                        
                        [emailBody appendString:appStr];
                        
                        if((i+1) == bookmarklist.count)
                        { 
                            [emailBody appendString:@"\n\n\n"];
                        }
                    }
                }
                else
                {
                    for(int i = 0; i < bookmarklist.count; i++)
                    {
                        Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                        
                        NSMutableString *appStr = [NSMutableString stringWithFormat:@"\n\n\n%d) %@\n\n%@\n\n%@", i+1, [NSString stringWithFormat:@"Title : %@", bookmark.bookmarktitle], [NSString stringWithFormat:@"Code : %@", bookmark.bookmarkcode], bookmark.bookmarkdescription];
                        
                        [emailBody appendString:appStr];
                        
                        if((i+1) == bookmarklist.count)
                        {
                            [emailBody appendString:@"\n\n\n"];
                        }
                    }
                }
            }
            else if(decider == 2)
            {
                [subject appendString:@"My Job - "];
                [subject appendString:jobTitle];
                
                int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
                productlist = [db retrieveProduct:myjobid];
                
                if(isneedprice)
                {
                    for(int i = 0; i < productlist.count; i++)
                    {
                        Product *product = [productlist objectAtIndex:i];
                        
                        NSMutableString *appStr = [NSMutableString stringWithFormat:@"\n\n\n%d) %@\n\n%@\n\n%@\n\n%@\n\n%@\n\n%@", i+1, [NSString stringWithFormat:@"Title : %@", product.producttitle], [NSString stringWithFormat:@"Code : %@", product.productcode], [NSString stringWithFormat:@"Trade: %@", product.producttrade], [NSString stringWithFormat:@"Retail: %@", product.productretail], [NSString stringWithFormat:@"Quantity : %d", product.productquantity], product.productdescription];
                        
                        [emailBody appendString:appStr];
                        
                        if((i+1) == productlist.count)
                        {
                            [emailBody appendString:@"\n\n\n"];
                        }
                    }
                    [emailBody appendString:[NSString stringWithFormat:@"Total Trade Price : $ %.2f\n", totaltrade]];
                    [emailBody appendString:[NSString stringWithFormat:@"Total Retail Price : $ %.2f", totalretail]];
                    [emailBody appendString:@"\n\n\n"];
                }
                else
                {
                    for(int i = 0; i < productlist.count; i++)
                    {
                        Product *product = [productlist objectAtIndex:i];
                        
                        NSMutableString *appStr = [NSMutableString stringWithFormat:@"\n\n\n%d) %@\n\n%@\n\n%@\n\n%@", i+1, [NSString stringWithFormat:@"Title : %@", product.producttitle], [NSString stringWithFormat:@"Code : %@", product.productcode], [NSString stringWithFormat:@"Quantity : %d", product.productquantity], product.productdescription];
                        
                        [emailBody appendString:appStr];
                        
                        if((i+1) == productlist.count)
                        {
                            [emailBody appendString:@"\n\n\n"];
                        }
                    }
                }
            }
            else
            {
                NSLog(@"error");
                
                return;
            }
            
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            [mailer setSubject:subject];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            [mailer setMessageBody:emailBody isHTML:NO];
            
            // only for iPad
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];
            
            [mailer release];
            [subject release];
            [emailBody release];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            [alert release];
        }
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
}

-(IBAction)sendhtml:(id)sender
{
    if([GlobalFunction NetworkStatus])
    {
        if ([MFMailComposeViewController canSendMail])
        {
            NSMutableString *subject = [[NSMutableString alloc] init];
            NSMutableString *emailBody = [[NSMutableString alloc] init];
            
            if(decider == 1)
            {
                [subject appendString:@"CRH - My Bookmarks"];
                
                bookmarklist = [db retrieveBookmark];
                
                if(isneedprice)
                {
                    for(int i = 0; i < bookmarklist.count; i++)
                    {
                        Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                        
                        NSMutableString *appStr = [NSMutableString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%d) %@<br /><br />%@<br /><br />%@<br /><br />%@<br /><br />%@", bookmark.bookmarkimagepathM, i+1, [NSString stringWithFormat:@"Title : %@", bookmark.bookmarktitle],[NSString stringWithFormat:@"Code : %@", bookmark.bookmarkcode], [NSString stringWithFormat:@"Trade : %@", bookmark.bookmarktrade], [NSString stringWithFormat:@"Retail : %@", bookmark.bookmarkretail], bookmark.bookmarkdescription];
                        
                        [emailBody appendString:appStr];
                        
                        if((i+1) == bookmarklist.count)
                        {
                            [emailBody appendString:@"<br /><br /><br />"];
                        }
                    }
                }
                else
                {
                    for(int i = 0; i < bookmarklist.count; i++)
                    {
                        Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                        
                        NSMutableString *appStr = [NSMutableString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%d) %@<br /><br />%@<br /><br />%@", bookmark.bookmarkimagepathM, i+1, [NSString stringWithFormat:@"Title : %@", bookmark.bookmarktitle], [NSString stringWithFormat:@"Code : %@", bookmark.bookmarkcode], bookmark.bookmarkdescription];
                        
                        [emailBody appendString:appStr];
                        
                        if((i+1) == bookmarklist.count)
                        {
                            [emailBody appendString:@"<br /><br /><br />"];
                        }
                    }
                }
            }
            else if(decider == 2)
            {
                [subject appendString:@"My Job - "];
                [subject appendString:jobTitle];
                
                int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
                productlist = [db retrieveProduct:myjobid];
                
                if(isneedprice)
                {
                    for(int i = 0; i < productlist.count; i++)
                    {
                        Product *product = [productlist objectAtIndex:i];
                        
                        NSMutableString *appStr = [NSMutableString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%d) %@<br /><br />%@<br /><br />%@<br /><br />%@<br /><br />%@<br /><br />%@", product.productimagepathM, i+1, [NSString stringWithFormat:@"Title : %@", product.producttitle],[NSString stringWithFormat:@"Code : %@", product.productcode], [NSString stringWithFormat:@"Trade : %@", product.producttrade], [NSString stringWithFormat:@"Retail : %@", product.productretail], [NSString stringWithFormat:@"Quantity : %d", product.productquantity], product.productdescription];
                        
                        [emailBody appendString:appStr];
                        
                        if((i+1) == productlist.count)
                        {
                            [emailBody appendString:@"<br /><br /><br />"];
                        }
                    }
                    [emailBody appendString:[NSString stringWithFormat:@"Total Trade Price : $ %.2f<br />", totaltrade]];
                    [emailBody appendString:[NSString stringWithFormat:@"Total Retail Price : $ %.2f", totalretail]];
                    [emailBody appendString:@"<br /><br /><br />"];
                }
                else
                {
                    for(int i = 0; i < productlist.count; i++)
                    {
                        Product *product = [productlist objectAtIndex:i];
                        
                        NSMutableString *appStr = [NSMutableString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%d) %@<br /><br />%@<br /><br />%@<br /><br />%@", product.productimagepathM, i+1, [NSString stringWithFormat:@"Title : %@", product.producttitle], [NSString stringWithFormat:@"Code : %@", product.productcode], [NSString stringWithFormat:@"Quantity : %d", product.productquantity], product.productdescription];
                        
                        [emailBody appendString:appStr];
                        
                        if((i+1) == productlist.count)
                        {
                            [emailBody appendString:@"<br /><br /><br />"];
                        }
                    }
                }
            }
            else
            {
                NSLog(@"error");
                
                return;
            }
            
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            [mailer setSubject:subject];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            [mailer setMessageBody:emailBody isHTML:YES];
            
            // only for iPad
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];
            
            [mailer release];
            [subject release];
            [emailBody release];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            [alert release];
        }
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
}

-(IBAction)sendpdf:(id)sender
{
    if([GlobalFunction NetworkStatus])
    {
        [GlobalFunction AddLoadingScreen:self];
        
        [self.view makeToast:@"Generating PDF..." duration:(0.3) position:@"center"];
        
        [self performSelector:@selector(delayPDF) withObject:nil afterDelay:0.3];
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
}

-(void)delayPDF
{
    //NSLog(@"call me when u r generating pdf file !!! n... me is mainScene");
    
    if([GlobalFunction NetworkStatus])
    {
        if ([MFMailComposeViewController canSendMail])
        {
            NSString * timeStampValue = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
            
            NSMutableString *subject = [[NSMutableString alloc] init];
            
            NSMutableString *requestString = [[NSMutableString alloc] init];
            
            WebServices *ws = [[WebServices alloc] init];
            
            NSString *reply = [[NSString alloc] init];
            
            NSMutableString *emailBody = [[NSMutableString alloc] init];
            
            if(decider == 1)
            {
                [subject appendString:@"CRH - My Bookmarks"];
                
                bookmarklist = [db retrieveBookmark];
                
                 if(isneedprice)
                 {
                     [requestString appendString:@"["];
                     
                     for(int i = 0; i < bookmarklist.count; i++)
                     {
                          Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                         
                          NSString *filterStr = [bookmark.bookmarkimagepathM stringByReplacingOccurrencesOfString:@"http://crh-app.novafusion.net:8052" withString:@".."];
                         
                         NSString *filterDescStr = [bookmark.bookmarkdescription stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
                         
                         filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                         filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºF" withString:@"fahrenheitSign"];
                         filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"º" withString:@"degreeSign"];
                         filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"®" withString:@"(REGISTERED)"];
                         
                         NSString *filterTitleStr = [bookmark.bookmarktitle stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                         
                          NSString *bookmarkString = [NSString stringWithFormat:@"{\"Title\":\"%@\",\"Code\":\"%@\",\"Trade\":\"%@\",\"Retail\":\"%@\",\"Description\":\"%@\",\"ImagePath\":\"%@\",\"TimeStamp\":\"%@\"}", filterTitleStr, bookmark.bookmarkcode, bookmark.bookmarktrade, bookmark.bookmarkretail, filterDescStr, filterStr, timeStampValue];
                         
                         [requestString appendString:bookmarkString];
                         
                         if((i+1) != bookmarklist.count)
                         {
                             [requestString appendString:@","];
                         }
                     }
                     
                     [requestString appendString:@"]"];
                     
                     reply = [ws generateListOfItemsPDFWithPrice:requestString];
                 }
                 else
                 {
                     [requestString appendString:@"["];
                     
                     for(int i = 0; i < bookmarklist.count; i++)
                     {
                         Bookmark *bookmark = [bookmarklist objectAtIndex:i];
                         
                         NSString *filterStr = [bookmark.bookmarkimagepathM stringByReplacingOccurrencesOfString:@"http://crh-app.novafusion.net:8052" withString:@".."];
                         
                         NSString *filterDescStr = [bookmark.bookmarkdescription stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
                         
                         filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                         filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºF" withString:@"fahrenheitSign"];
                         filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"º" withString:@"degreeSign"];
                         filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"®" withString:@"(REGISTERED)"];
                         
                         NSString *filterTitleStr = [bookmark.bookmarktitle stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                         
                         NSString *bookmarkString = [NSString stringWithFormat:@"{\"Title\":\"%@\",\"Code\":\"%@\",\"Description\":\"%@\",\"ImagePath\":\"%@\",\"TimeStamp\":\"%@\"}", filterTitleStr, bookmark.bookmarkcode, filterDescStr, filterStr, timeStampValue];
                         
                         [requestString appendString:bookmarkString];
                         
                         if((i+1) != bookmarklist.count)
                         {
                             [requestString appendString:@","];
                         }
                     }
                     
                     [requestString appendString:@"]"];
                     
                     reply = [ws generateListOfItemsPDFWithoutPrice:requestString];
                 }
            }
            else if(decider == 2)
            {
                [subject appendString:@"My Job - "];
                [subject appendString:jobTitle];
                
                int myjobid = [db retrieveMyjobselectedrow:jobrowselected];
                productlist = [db retrieveProduct:myjobid];
                
                if(isneedprice)
                {
                    [requestString appendString:@"["];
                    
                    for(int i = 0; i < productlist.count; i++)
                    {
                        Product *product = [productlist objectAtIndex:i];
                        
                        NSString *filterStr = [product.productimagepathM stringByReplacingOccurrencesOfString:@"http://crh-app.novafusion.net:8052" withString:@".."];
                        
                        NSString *filterDescStr = [product.productdescription stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
                        
                        filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                        filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºF" withString:@"fahrenheitSign"];
                        filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"º" withString:@"degreeSign"];
                        filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"®" withString:@"(REGISTERED)"];
                        
                        NSString *filterTitleStr = [product.producttitle stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                        
                        NSString *productString = [NSString stringWithFormat:@"{\"Title\":\"%@\",\"Code\":\"%@\",\"Trade\":\"%@\",\"Retail\":\"%@\",\"Description\":\"%@\",\"ImagePath\":\"%@\",\"TimeStamp\":\"%@\",\"Quantity\":\"%d\",\"TotalTrade\":\"%@\",\"TotalRetail\":\"%@\"}", filterTitleStr, product.productcode, product.producttrade, product.productretail, filterDescStr, filterStr, timeStampValue, product.productquantity, [NSString stringWithFormat:@"%.2f", totaltrade], [NSString stringWithFormat:@"%.2f", totalretail]];
                        
                        [requestString appendString:productString];
                        
                        if((i+1) != productlist.count)
                        {
                            [requestString appendString:@","];
                        }
                    }
                    
                     [requestString appendString:@"]"];
                    
                     reply = [ws generateListOfItemsPDFWithPrice:requestString];
                }
                else
                {
                    [requestString appendString:@"["];
                    
                    for(int i = 0; i < productlist.count; i++)
                    {
                        Product *product = [productlist objectAtIndex:i];
                        
                        NSString *filterStr = [product.productimagepathM stringByReplacingOccurrencesOfString:@"http://crh-app.novafusion.net:8052" withString:@".."];
                        
                        NSString *filterDescStr = [product.productdescription stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
                        
                        filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                        filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºF" withString:@"fahrenheitSign"];
                        filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"º" withString:@"degreeSign"];
                        filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"®" withString:@"(REGISTERED)"];
                        
                        NSString *filterTitleStr = [product.producttitle stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
                        
                        NSString *productString = [NSString stringWithFormat:@"{\"Title\":\"%@\",\"Code\":\"%@\",\"Description\":\"%@\",\"ImagePath\":\"%@\",\"TimeStamp\":\"%@\",\"Quantity\":\"%d\"}", filterTitleStr, product.productcode, filterDescStr, filterStr, timeStampValue, product.productquantity];
                        
                        [requestString appendString:productString];
                        
                        if((i+1) != productlist.count)
                        {
                            [requestString appendString:@","];
                        }
                    }
                    
                    [requestString appendString:@"]"];
                    
                    reply = [ws generateListOfItemsPDFWithoutPrice:requestString];
                }
            }
            else
            {
                NSLog(@"error");
                
                return;
            }
            
            //onli when the return is success will display the email editor sheet !!!!
            
            if([reply isEqualToString:@"Success"])
            {
                NSString *pdf_link = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/tiseno/fpdf17/product_pdf/%@.pdf", timeStampValue];
                
                MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
                mailer.mailComposeDelegate = self;
            
                [mailer setSubject:subject];
            
                [emailBody appendString:[NSString stringWithFormat:@"<br /><br /><br /><br />Click the link to download the pdf file : <a href=\"%@\">%@.pdf</a>", pdf_link, timeStampValue]];
                
                NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
                [mailer setToRecipients:toRecipients];
            
                [mailer setMessageBody:emailBody isHTML:YES];
            
                // only for iPad
                mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
                [self presentModalViewController:mailer animated:YES];
            
                [mailer release];
                [subject release];
                [ws release];
                [reply release];
                [emailBody release];
            }
            else
            {
                [self.view makeToast:@"Failed to generate PDF..." duration:(0.3) position:@"center"];
            }
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            [alert release];
        }
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
    
    [GlobalFunction RemoveLoadingScreen:self];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController*)popoverController
{
    sendpop = nil;
    emailpop = nil;
}

@end
