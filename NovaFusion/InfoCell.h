//
//  InfoCell.h
//  NovaFusion
//
//  Created by tiseno on 10/4/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoCell : UITableViewCell{
    
}

@property (nonatomic, retain) UILabel *titlelabel, *codelabel, *tradelabel, *retaillabel, *quantitylabel;
@property (nonatomic, retain) UIImageView *productimage, *quantityimage;

@end
