typedef enum
{
    DataBaseConnectionOpened,
    DataBaseConnectionFailed
} DataBaseConncetionOpenResult;

typedef enum 
{
    DataBaseInsertionSuccessful,
    DataBaseInsertionFailed
} DataBaseInsertionResult;
typedef enum 
{
    DataBaseUpdateSuccessful,
    DataBaseUpdateFailed
} DataBaseUpdateResult;
typedef enum 
{
    DataBaseDeletionSuccessful,
    DataBaseDeletionFailed
} DataBaseDeletionResult;