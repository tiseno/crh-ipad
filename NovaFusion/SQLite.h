//
//  SQLite.h
//  OrientalDailyiOS
//
//  Created by Tiseno on 8/9/12.
//  Copyright (c) 2012 Tiseno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

#import "DataBaseConnectionOpenResult.h"

@interface SQLite : NSObject{
    sqlite3 *dbKDS;
    NSString *databaseName;
}

-(id)initWithDataBaseName:(NSString*)idataBaseName;
-(DataBaseConncetionOpenResult)openConnection;
-(void)closeConnection;

@end
