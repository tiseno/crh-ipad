//
//  WebServices.h
//  NovaFusion
//
//  Created by Jermin Bazazian on 10/2/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "nova_application.h"

@interface WebServices : NSObject

- (NSArray*) selectAllFromApplicationTableExceptParam;
- (NSArray*) selectAllFromCategoryTableExceptParam: (int) appID;
- (NSArray*) selectAllFromCategoryItemTableByID: (int) catID;
- (NSArray*) selectItemFromItemTableByID: (int) itemID;
- (NSArray*) selectGalleryImageByPath: (NSString*) imagePath;
- (NSArray*) selectTechnicalImageByPath: (NSString*) imagePath;
- (NSString*) sendEnquiry: (NSString*) FirstName :(NSString*) LastName :(NSString*) Email :(NSString*) Phone :(NSString*) Company :(NSString*) Address :(NSString*) ProductCode :(NSString*) Subscribe :(NSString*) Comments;
- (NSString*) generatePDF_FileWithPrice:(NSString*) Title :(NSString*) Code :(NSString*) Trade :(NSString*) Retail :(NSString*) Description : (NSString*) ImagePath: (NSString*) TimeStamp;
- (NSString*) generatePDF_FileWithoutPrice:(NSString*) Title :(NSString*) Code :(NSString*) Description : (NSString*) ImagePath : (NSString*) TimeStamp;

- (NSArray*) searchItemByKeywords: (NSString*) SearchStr;
- (NSArray*) selectAllCategoryItem: (int) itemID;
- (NSArray*) selectApplicationNameByID: (int) appID;
- (NSArray*) selectCategoryIDByID: (int) itemID;
- (NSString*) generateListOfItemsPDFWithPrice:(NSMutableString*) requestString;
- (NSString*) generateListOfItemsPDFWithoutPrice:(NSMutableString*) requestString;
- (NSArray*) searchItemByBarCode: (NSString*) BarCode;
- (NSArray*) getParentKeyByItemID: (NSString*) ParentKey;

@end
