//
//  Bookmark.m
//  NovaFusion
//
//  Created by tiseno on 10/9/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import "Bookmark.h"

@implementation Bookmark

@synthesize bookmarktitle, bookmarkcode, bookmarktrade, bookmarkretail, bookmarkdescription, bookmarkimagepathS, bookmarkimagepathM, sectionid, categoryid;

@end
