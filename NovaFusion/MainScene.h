//
//  MainScene.h
//  NovaFusion
//
//  Created by tiseno on 9/28/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "ZBarSDK.h"
#import "GlobalFunction.h"
#import "WebServices.h"
#import "DatabaseAction.h"
#import "ChildScene.h"
#import "SearchScene.h"
#import "SectionCell.h"
#import "BookmarkCell.h"
#import "MyJobCell.h"
#import "InfoCell.h"
#import "HelpCell.h"
#import "Bookmark.h"
#import "Myjob.h"
#import "Product.h"
#import "AppDelegate.h"

@interface MainScene : UIViewController<UITableViewDataSource, UITableViewDelegate, ZBarReaderDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, UISearchBarDelegate, UIPopoverControllerDelegate, MFMailComposeViewControllerDelegate>{
    SectionCell *sectioncell;
    BookmarkCell *bookmarkcell;
    MyJobCell *myjobcell;
    InfoCell *infocell;
    HelpCell *helpcell;
    BOOL isneedprice, iseditbookmark, iseditmyjob, isbookmark, ismyjob, isinfojob, isconnected;
    int row, jobrowselected, tagbutton, decider;
    float totalretail, totaltrade;
    UIAlertView *alert;
    UITextField *newjobtitletextfield;
    
    NSArray *joblist, *productlist, *bookmarklist;
    DatabaseAction *db;
    
    NSString *titletext, *codetext, *retailtext, *tradetext, *imagepathtext;
    UIView *gesturetapview;
    NSTimer *connectiontimer;
    UIPopoverController *emailpop, *sendpop;
    NSString *data;
}

//main//
@property (nonatomic, retain) UIImagePickerController *imgPicker;
@property (retain, nonatomic) IBOutlet UITableView *sectiontable;
@property (retain, nonatomic) IBOutlet UISearchBar *searchbar;
@property (retain, nonatomic) IBOutlet UIView *subview;

-(IBAction)about:(id)sender;
-(IBAction)contactus:(id)sender;
-(IBAction)help:(id)sender;
-(IBAction)myjobs:(id)sender;
-(IBAction)bookmarks:(id)sender;
-(IBAction)scan:(id)sender;
-(IBAction)launchsafari:(id)sender;

@property (retain, nonatomic) NSArray *mainArr;
///////

//my job//
@property (retain, nonatomic) IBOutlet UIView *myjobview;
@property (retain, nonatomic) IBOutlet UITableView *myjobtable;
@property (retain, nonatomic) IBOutlet UITextField *jobtitletextfield;
@property (retain, nonatomic) IBOutlet UILabel *myjobtitlelabel;

-(IBAction)addnewjob:(id)sender;
-(IBAction)deletejob:(id)sender;
/////////

//bookmarks//
@property (retain, nonatomic) IBOutlet UIView *bookmarkview;
@property (retain, nonatomic) IBOutlet UITableView *bookmarktable;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *editbookmarkbutton;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *donebookmarkbutton;
@property (retain, nonatomic) IBOutlet UIButton *emailbookmarkbutton;
@property (retain, nonatomic) IBOutlet UILabel *bookmarktitlelabel;

-(IBAction)editbookmark:(id)sender;
-(IBAction)donebookmark:(id)sender;
////////////

//info//
@property (retain, nonatomic) IBOutlet UIView *infoview;
@property (retain, nonatomic) IBOutlet UITableView *infotable;
@property (retain, nonatomic) IBOutlet UILabel *joblabel;
@property (retain, nonatomic) IBOutlet UIView *totalcostview;
@property (retain, nonatomic) IBOutlet UILabel *tradelabel;
@property (retain, nonatomic) IBOutlet UILabel *retaillabel;
@property (retain, nonatomic) IBOutlet UIView *editjobview;
@property (retain, nonatomic) IBOutlet UIToolbar *toolbar;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *editjobinfobutton;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *donejobinfobutton;
@property (retain, nonatomic) IBOutlet UIButton *emailjobinfobutton;
@property (retain, nonatomic) IBOutlet UILabel *infotitlelabel;

-(IBAction)editjobinfo:(id)sender;
-(IBAction)donejobinfo:(id)sender;
///////

//about//
@property (retain, nonatomic) IBOutlet UIView *achview;
@property (retain, nonatomic) IBOutlet UILabel *titlelabel;
@property (retain, nonatomic) IBOutlet UILabel *descriptionlabel;
@property (retain, nonatomic) IBOutlet UILabel *description2label;

-(IBAction)close:(id)sender;
//////////////////////////

//contact us//
@property (retain, nonatomic) IBOutlet UIView *contactusview;
@property (retain, nonatomic) IBOutlet UILabel *contactdescriptionlabel;

-(IBAction)closecontactus:(id)sender;
-(IBAction)emailcontactus:(id)sender;
-(IBAction)facebookcontactus:(id)sender;
-(IBAction)linkedcontactus:(id)sender;

//help//
@property (retain, nonatomic) IBOutlet UIView *helpview;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundimage;
@property (retain, nonatomic) IBOutlet UITableView *helptable;

-(IBAction)closehelp:(id)sender;
////////

//popover//
@property (retain, nonatomic) IBOutlet UIViewController *emailcontroller;
@property (retain, nonatomic) IBOutlet UIViewController *sendcontroller;

-(IBAction)popup:(id)sender;
-(IBAction)sendwithprice:(id)sender;
-(IBAction)sendwithoutprice:(id)sender;
-(IBAction)sendplain:(id)sender;
-(IBAction)sendhtml:(id)sender;
-(IBAction)sendpdf:(id)sender;
//////////

@property (retain, nonatomic) NSMutableArray *itemNameArr, *itemCodeArr, *itemPriceArr, *itemImageArr, *itemAppIdArr, *itemIdArr;

@property (retain, nonatomic) NSString *jobTitle;

@end
