//
//  nova_application.m
//  NovaFusion
//
//  Created by Jermin Bazazian on 10/2/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import "nova_application.h"

@implementation nova_application

@synthesize name, alias, application_group, description, params;

-(void)dealloc
{
    //[img_Numbers, anws, total_Possibility, poss_3, poss_4 release];
    
    [super dealloc];
}

@end
