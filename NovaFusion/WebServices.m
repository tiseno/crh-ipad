//
//  WebServices.m
//  NovaFusion
//
//  Created by Jermin Bazazian on 10/2/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import "WebServices.h"
#import "nova_application.h"

@interface WebServices()

@property (nonatomic, strong) NSMutableData *receivedData;

@end

@implementation WebServices

@synthesize receivedData = receivedData_;

- (void)viewDidLoad
{
    //[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //initialize receivedData_
    receivedData_ = [[NSMutableData alloc] init];
}

- (void)viewDidUnload
{
    //[super viewDidUnload];
    // Release any retained subviews of the main view.
}

/**** NSURLConnectionDelegate Async callbacks : START ****/
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"No internet access !!!");
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    
    int code = [httpResponse statusCode];
    
    if(code > 400){NSLog(@"Server error !!");}
    
    [receivedData_ setLength:0];
}

- (void) connection: (NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData_ appendData:data];
}

- (void) connectionDidFinishLoading: (NSURLConnection *) connection
{
    NSString* strResponse = [[NSString alloc] initWithData:receivedData_ encoding:NSUTF8StringEncoding];
    
    NSLog(@"Query status : %@", strResponse);
    
    [strResponse release];
    
    receivedData_ = nil;
}
/**** NSURLConnectionDelegate Async callbacks : END ****/

- (NSArray*) selectAllFromApplicationTableExceptParam
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"RequestID\":\"%d\"}", 0];
    
    NSLog(@"Request: %@", jsonRequest);

    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectAllFromApplicationTableExceptParam&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];

    //output the result in string
    NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self novaResultData:POSTReply];
    
    return nova_result;
}

- (NSArray*)novaResultData:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    
    //NSLog(@"app arr : %@", novaArray);
    
    /*
        "content.teaser_description": "Door Latches, Locks, Handles & Hinges, Sliding Door Hardware, Pressure Relief Valves, Safety Alarms and Bells, Suspended Ceiling Hardware, Condensate Evaporators.\r\n",
        "content.teaser_image": "images\/products\/refrigeration_hardware\/cabinet_latches_and_strikes\/C-DLA-LHC\/gallery\/C-DLA-LHC_01.jpg",
        "content.teaser_image_width": "150",
        "content.teaser_image_height": "150",
     */
    
    //loop the result passed back
    /*for(int i = 0; i < novaArray.count; i++)
    {
        NSDictionary* novainfo = [novaArray objectAtIndex:i];
        
        NSDictionary* novadetails = [novainfo objectForKey:@"application"];
        
        NSDictionary *details = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"params"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        NSLog(@"item ... %@", [details objectForKey:@"content.teaser_description"]);
        
        //actual link is -> http://crh-app.novafusion.net:8052/images/products/refrigeration_hardware/cabinet_latches_and_strikes/C-DLA-LHC/gallery/C-DLA-LHC_01.jpg
        //so the link to photo need to add http://crh-app.novafusion.net:8052/
        
        NSLog(@"item ... %@", [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [details objectForKey:@"content.teaser_image"]]);
        
        //width and height are needed because the link is big image in gallery
        
        NSLog(@"item ... %@", [details objectForKey:@"content.teaser_image_width"]);
        NSLog(@"item ... %@", [details objectForKey:@"content.teaser_image_height"]);
    }*/
    
    return novaArray;
}

- (NSArray*) selectAllFromCategoryTableExceptParam: (int) appID
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"appID\":\"%d\"}", appID];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectAllFromCategoryTableExceptParam&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self novaCategoryResultData:POSTReply];
    
    return nova_result;
}

- (NSArray*)novaCategoryResultData:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    
    //loop the result passed back
    //for(int i = 0; i < novaArray.count; i++)
    //{
    //    NSDictionary* novainfo = [novaArray objectAtIndex:i];
    
    //    NSDictionary* novadetails = [novainfo objectForKey:@"category"];
    
    //    NSLog(@"app ID : %@", [novadetails objectForKey:@"application_id"]);
    //}
    
    return novaArray;
}

- (NSArray*) selectAllFromCategoryItemTableByID: (int) catID
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"catID\":\"%d\"}", catID];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectAllFromCategoryItemTableByID&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self novaCategoryItemResultData:POSTReply];
    
    return nova_result;
}

- (NSArray*)novaCategoryItemResultData:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    
    //loop the result passed back
    //for(int i = 0; i < novaArray.count; i++)
    //{
    //    NSDictionary* novainfo = [novaArray objectAtIndex:i];
    
    //    NSDictionary* novadetails = [novainfo objectForKey:@"category"];
    
    //    NSLog(@"app ID : %@", [novadetails objectForKey:@"application_id"]);
    //}
    
    return novaArray;
}

- (NSArray*) selectItemFromItemTableByID: (int) itemID
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"itemID\":\"%d\"}", itemID];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectItemFromItemTableByID&format=json"];
 
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self novaItemResultData:POSTReply];
    
    return nova_result;
}

- (NSArray*)novaItemResultData:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    
    //loop the result passed back
    /*for(int i = 0; i < novaArray.count; i++)
    {
        NSDictionary* novainfo = [novaArray objectAtIndex:i];
    
        NSDictionary* novadetails = [novainfo objectForKey:@"item"];
        
        NSDictionary *test = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        //Print the description of the product
        NSDictionary *test2 = [test objectForKey:@"98ac0b3a-035d-4367-97fb-033cd64f659a"];
        NSDictionary *test3 = [test2 objectForKey:@"0"];
        
        NSLog(@"item value : %@", [test3 objectForKey:@"value"]);
        
        //Print the price of the product
        NSDictionary *test4 = [test objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
        NSDictionary *test5 = [test4 objectForKey:@"0"];
        
        NSLog(@"item price : %@", [test5 objectForKey:@"value"]);
        
        //Get image path - 1
        NSDictionary *test6 = [test objectForKey:@"5f304277-9eff-4033-88b9-e231db615697"];
        
        NSLog(@"item image path 1 : %@", [test6 objectForKey:@"value"]);
        
        //Get image path - 2
        NSDictionary *test7 = [test objectForKey:@"35edcd61-decc-4ba2-9146-995bcbcb0245"];
        
        NSLog(@"item image path 2 : %@", [test7 objectForKey:@"value"]);
    }*/
    
    // code for the inc-gst
    /*for(int i = 0; i < novaArray.count; i++)
    {
        NSDictionary* novainfo = [novaArray objectAtIndex:i];
        
        NSDictionary* novadetails = [novainfo objectForKey:@"item"];
        
        NSDictionary *test = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        //Print the description of the product
        NSDictionary *test2 = [test objectForKey:@"695755a4-ae67-43bb-8c39-af4eb9ad4961"];
        NSDictionary *test3 = [test2 objectForKey:@"option"];
        
        NSLog(@"inc-gst here ... %@", [test3 objectForKey:@"0"]);
        
    }*/
    
    // newest code here
    /*for(int i = 0; i < novaArray.count; i++)
    {
        NSDictionary* novainfo = [novaArray objectAtIndex:i];
        
        NSDictionary* novadetails = [novainfo objectForKey:@"item"];
        
        NSDictionary *test = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        if([test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"] != nil)
        {
            NSDictionary *test2 = [test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"];
            
            NSDictionary *test3 = [test2 objectForKey:@"option"];
            
            NSLog(@"trade ... %@", [test3 objectForKey:@"0"]);
        }
    }*/
    
    return novaArray;
}

- (NSArray*) selectGalleryImageByPath: (NSString*) imagePath
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"imagePath\":\"%@\"}", imagePath];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectGalleryImageByPath&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self novaGalleryImage:POSTReply];
    
    return nova_result;
}

- (NSArray*)novaGalleryImage:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    
    //loop the result passed back
    /*for(int i = 0; i < novaArray.count; i++)
     {
     NSDictionary* novainfo = [novaArray objectAtIndex:i];
     
     NSDictionary* novadetails = [novainfo objectForKey:@"img"];
     
     NSLog(@"imgPath : %@", novadetails);
     }*/
    
    return novaArray;
}

- (NSArray*) selectTechnicalImageByPath: (NSString*) imagePath
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"imagePath\":\"%@\"}", imagePath];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectTechnicalImageByPath&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self novaTechnicalImage:POSTReply];
    
    return nova_result;
}

- (NSArray*)novaTechnicalImage:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    
    //loop the result passed back
    /*for(int i = 0; i < novaArray.count; i++)
     {
     NSDictionary* novainfo = [novaArray objectAtIndex:i];
     
     NSDictionary* novadetails = [novainfo objectForKey:@"img"];
     
     NSLog(@"imgPath : %@", novadetails);
     }*/
    
    return novaArray;
}

- (NSString*) sendEnquiry: (NSString*) FirstName :(NSString*) LastName :(NSString*) Email :(NSString*) Phone :(NSString*) Company :(NSString*) Address :(NSString*) ProductCode :(NSString*) Subscribe :(NSString*) Comments
{
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"FirstName\":\"%@\",\"LastName\":\"%@\",\"Email\":\"%@\",\"Phone\":\"%@\",\"Company\":\"%@\",\"Address\":\"%@\",\"ProductCode\":\"%@\",\"Subscribe\":\"%@\",\"Comments\":\"%@\"}", FirstName, LastName, Email, Phone, Company, Address, ProductCode, Subscribe, Comments];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=sendEnquiry&format=json"];
    
    //NSURL *url = [NSURL URLWithString:@"http://www.zrlim.com/tiseno/sendEnquiry.php?mName=sendEnquiry&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    return theReply;
}

- (NSString*) generatePDF_FileWithPrice:(NSString*) Title :(NSString*) Code :(NSString*) Trade :(NSString*) Retail :(NSString*) Description : (NSString*) ImagePath : (NSString*) TimeStamp
{
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"Title\":\"%@\",\"Code\":\"%@\",\"Trade\":\"%@\",\"Retail\":\"%@\",\"Description\":\"%@\",\"ImagePath\":\"%@\",\"TimeStamp\":\"%@\"}", Title, Code, Trade, Retail, Description, ImagePath, TimeStamp];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=generatePDF_FileWithPrice&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    return theReply;
}

- (NSString*) generatePDF_FileWithoutPrice:(NSString*) Title :(NSString*) Code :(NSString*) Description : (NSString*) ImagePath : (NSString*) TimeStamp
{
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"Title\":\"%@\",\"Code\":\"%@\",\"Description\":\"%@\",\"ImagePath\":\"%@\",\"TimeStamp\":\"%@\"}", Title, Code, Description, ImagePath, TimeStamp];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=generatePDF_FileWithoutPrice&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    return theReply;
}

- (NSArray*) searchItemByKeywords: (NSString*) SearchStr
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"SearchStr\":\"%@\"}", SearchStr];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=searchItemByKeywords&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self novaItemResultDataSearch:POSTReply];
    
    return nova_result;
}

- (NSArray*)novaItemResultDataSearch:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    NSLog(@"array size : %d", novaArray.count);
    
    /*
     for(int i = 0; i < novaArray.count; i++)
     {
     NSDictionary* novainfo = [novaArray objectAtIndex:i];
     
     NSDictionary* novadetails = [novainfo objectForKey:@"item"];
     
     NSLog(@"item name : %@", [novadetails objectForKey:@"name"]);
     
     NSDictionary *elementDetails = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
     
     //Print the price of the product
     NSDictionary *priceDetails = [elementDetails objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
     NSDictionary *price = [priceDetails objectForKey:@"0"];
     
     NSLog(@"item price : %@", [price objectForKey:@"value"]);
     
     NSDictionary *itemDic = [elementDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
     NSDictionary *itemCode = [itemDic objectForKey:@"0"];
     
     NSLog(@"item code : %@",[itemCode objectForKey:@"value"]);
     
     NSDictionary *thumbpath = [elementDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
     
     NSLog(@"thumb img path : %@", [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]);
     }
     */
    
    return novaArray;
}


- (NSArray*) selectAllCategoryItem: (int) itemID
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"itemID\":\"%d\"}", itemID];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectAllCategoryItem&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self novaItemList:POSTReply];
    
    return nova_result;
}

- (NSArray*)novaItemList:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    
    //loop the result passed back
    /*for(int i = 0; i < novaArray.count; i++)
     {
     NSDictionary* novainfo = [novaArray objectAtIndex:i];
     
     NSDictionary* novadetails = [novainfo objectForKey:@"item"];
     
     NSLog(@"item id : %@", [novadetails objectForKey:@"item_id"]);
     }*/
    
    return novaArray;
}

- (NSArray*) selectApplicationNameByID: (int) appID
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"appID\":\"%d\"}", appID];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectApplicationNameByID&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self categoryTitle:POSTReply];
    
    return nova_result;
}

- (NSArray*)categoryTitle:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    
    return novaArray;
}

- (NSArray*) selectCategoryIDByID: (int) itemID
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"itemID\":\"%d\"}", itemID];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=selectCategoryIDByID&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self categoryID:POSTReply];
    
    return nova_result;
}

- (NSArray*)categoryID:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    
    return novaArray;
}

- (NSString*) generateListOfItemsPDFWithPrice:(NSMutableString*) requestString
{
    NSMutableString *jsonRequest = requestString;
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=generateListOfItemsPDFWithPrice&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    return theReply;
}

- (NSString*) generateListOfItemsPDFWithoutPrice:(NSMutableString*) requestString
{
    NSMutableString *jsonRequest = requestString;
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=generateListOfItemsPDFWithoutPrice&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    return theReply;
}

- (NSArray*) searchItemByBarCode: (NSString*) BarCode
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"BarCode\":\"%@\"}", BarCode];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=searchItemByBarCode&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self novaItemResultDataBarCode:POSTReply];
    
    return nova_result;
}

- (NSArray*)novaItemResultDataBarCode:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    //NSLog(@"array size : %d", novaArray.count);
    
     //for(int i = 0; i < novaArray.count; i++)
     //{
     //NSDictionary* novainfo = [novaArray objectAtIndex:i];
     
     //NSDictionary* novadetails = [novainfo objectForKey:@"item"];
     
     //NSLog(@"item name : %@", [novadetails objectForKey:@"name"]);
     
     //NSDictionary *elementDetails = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
     
     //Print the price of the product
     //NSDictionary *priceDetails = [elementDetails objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
     //NSDictionary *price = [priceDetails objectForKey:@"0"];
     
     //NSLog(@"item price : %@", [price objectForKey:@"value"]);
     
     //NSDictionary *itemDic = [elementDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
     //NSDictionary *itemCode = [itemDic objectForKey:@"0"];
     
     //NSLog(@"item code : %@",[itemCode objectForKey:@"value"]);
     
     //NSDictionary *thumbpath = [elementDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
     
     //NSLog(@"thumb img path : %@", [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]);
     //}
     
    
    return novaArray;
}

- (NSArray*) getParentKeyByItemID: (NSString*) ParentKey
{
    NSArray *nova_result = [[NSArray alloc] init];
    
    NSString *jsonRequest = [NSString stringWithFormat:@"{\"ParentKey\":\"%@\"}", ParentKey];
    
    NSLog(@"Request: %@", jsonRequest);
    
    NSURL *url = [NSURL URLWithString:@"http://crh-app.novafusion.net:8052/tiseno/request_details.php?mName=getParentKeyByItemID&format=json"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    //use delegate method for error handling, response and status returned
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    //response returned by the server
    NSURLResponse *response;
    
    NSData *POSTReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //output the result in string
    //NSString *theReply = [[NSString alloc] initWithBytes:[POSTReply bytes] length:[POSTReply length] encoding: NSASCIIStringEncoding];
    //NSLog(@"Reply: %@", theReply);
    
    nova_result = [self novaItemResultParentKey:POSTReply];
    
    return nova_result;
}

- (NSArray*)novaItemResultParentKey:(NSData *)responseData
{
    //parse out the json data
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* novaArray = [json objectForKey:@"posts"];
    
    /*for(int i = 0; i < novaArray.count; i++)
    {
        NSDictionary* novainfo = [novaArray objectAtIndex:i];
        
        NSLog(@"... ... %@", [novainfo objectForKey:@"parent"]);
    }*/
    
    return novaArray;
}
@end

/*
 NSString *s = @"foo/bar:baz.foo";
 NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"/:."];
 s = [[s componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
 NSLog(@"%@", s); // => foobarbazfoo
 */
