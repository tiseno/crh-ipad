//
//  GlobalFunction.m
//  NovaFusion
//
//  Created by tiseno on 9/28/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import "GlobalFunction.h"

@implementation GlobalFunction

+(BOOL)NetworkStatus
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus==NotReachable);
}

+(void)AddLoadingScreen:(UIViewController *)viewcontroller
{
    UIView *existingToast = [viewcontroller.view viewWithTag:1054];
    if (existingToast != nil) {
        [existingToast removeFromSuperview];
    }
    
    UIView *activityContainer = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width)] autorelease];

    [activityContainer setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]];
    [activityContainer setTag:1054];
    [activityContainer setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin)];
    
    UIActivityIndicatorView *activityView = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]autorelease];
    [activityView setCenter:CGPointMake(activityContainer.bounds.size.width / 2, activityContainer.bounds.size.height / 2)];
    [activityContainer addSubview:activityView];
    [activityView startAnimating];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView commitAnimations];
    
    [viewcontroller.view addSubview:activityContainer];
}

+(void)RemoveLoadingScreen:(UIViewController *)viewcontroller
{
    UIView *existingToast = [viewcontroller.view viewWithTag:1054];
    if (existingToast != nil) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationDelegate:existingToast];
        [UIView setAnimationDidStopSelector:@selector(removeFromSuperview)];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [existingToast setAlpha:0.0];
        [UIView commitAnimations];
    }
}

@end
