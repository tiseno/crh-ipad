//
//  MyJobCell.h
//  NovaFusion
//
//  Created by tiseno on 10/4/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyJobCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *myjobtitle, *myjobdate;
@property (nonatomic, retain) UIImageView *arrowimage;

@end
