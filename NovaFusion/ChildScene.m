//
//  ChildScene.m
//  NovaFusion
//
//  Created by tiseno on 9/28/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#define CategoryTableHeight 39
#define ProductTableHeight 105
#define MainTableWidth 300
#define JobTableWidth 250
#define JobTableHeight 40

#import "ChildScene.h"

@implementation ChildScene

@synthesize categoryview, categoryheaderbg, categorytable, productview, producttable, thumbnailtable, borderimage, productimage, buttonbgimage, detailscrollview, titlelabel, codelabel, tradelabel, retaillabel, detailview, headerlabel, categorylabel, seriallabel, titlebgimage, searchview, searchbar, addjobbutton, enquirybutton, emailbutton, categoryArr, categorytext, itemArr, row, totalItem, itemPriceArr, middleImageArr, tradeArr, technicalImageArr, galleryImageArr;

@synthesize popupview, popupimage, popuplabel, popuppreviousbutton, popupnextbutton, popupclosebutton, loadingview;

@synthesize popcodelabel, popdescriptionlabel, jobtable;

@synthesize enquiryview, popfirstnametextfield, poplastnametextfield, popemailtextfield, popaddresstextfield, popcompanytextfield, popphonetextfield, popproductcodetextfield, popcommentstextview, popselectedbutton, popnotselectedbutton, popscrollview;

@synthesize addjobcontroller, emailcontroller, sendcontroller;

@synthesize itemID, appID, viewOption;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    transparentview = [[[UIView alloc]init] autorelease];
    transparentview.backgroundColor = [UIColor clearColor];
    self.categorytable.tableFooterView = transparentview;
    self.producttable.tableFooterView = transparentview;
    self.jobtable.tableFooterView = transparentview;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    self.categoryview.backgroundColor = [UIColor clearColor];
    self.productview.backgroundColor = [UIColor clearColor];
    self.detailview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    self.categorytable.backgroundColor = [UIColor blackColor];
    self.producttable.backgroundColor = [UIColor whiteColor];
    
    self.categorylabel.text = self.categorytext;
    
    beforesearch = self.categoryArr;
    
    self.popscrollview.contentSize = CGSizeMake(420, 410);
    
    issubcribe = YES;
    self.popselectedbutton.enabled = NO;
    
    db = [[DatabaseAction alloc] init];
    
    if([viewOption intValue] == 1)
    {
        [GlobalFunction AddLoadingScreen:self];
        [self performSelector:@selector(delay7) withObject:nil afterDelay:0.1];
    }
    else
    {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        red = [[appDelegate.colorcodered objectAtIndex:row] floatValue];
        green = [[appDelegate.colorcodegreen objectAtIndex:row] floatValue];
        blue = [[appDelegate.colorcodeblue objectAtIndex:row] floatValue];
        
        self.categoryheaderbg.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.categoryheaderbg.frame;
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1] CGColor], [appDelegate.gradientbottom objectAtIndex:row], nil];
        [self.categoryheaderbg.layer insertSublayer:gradient atIndex:0];
        self.categoryview.hidden = NO;
        rowselected = -1;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[popcommentstextview layer] setBorderWidth:1];
    [[popcommentstextview layer] setCornerRadius:5];
    
    popfirstnametextfield.enablesReturnKeyAutomatically = NO;
    poplastnametextfield.enablesReturnKeyAutomatically = NO;
    popemailtextfield.enablesReturnKeyAutomatically = NO;
    popaddresstextfield.enablesReturnKeyAutomatically = NO;
    popcompanytextfield.enablesReturnKeyAutomatically = NO;
    popphonetextfield.enablesReturnKeyAutomatically = NO;
    popcommentstextview.enablesReturnKeyAutomatically = NO;
    
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
    
    [[self.searchbar.subviews objectAtIndex:0] removeFromSuperview];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
}

-(void)dealloc
{
    [totalItem release];
    [itemArr release];
    [itemPriceArr release];
    [middleImageArr release];
    [tradeArr release];
    [technicalImageArr release];
    [galleryImageArr release];
    
    [categoryview release];
    [categoryheaderbg release];
    [categorytable release];
    [productview release];
    [producttable release];
    [thumbnailtable release];
    [borderimage release];
    [productimage release];
    [buttonbgimage release];
    [detailscrollview release];
    [titlelabel release];
    [codelabel release];
    [tradelabel release];
    [retaillabel release];
    [detailview release];
    [headerlabel release];
    [categorylabel release];
    [seriallabel release];
    [titlebgimage release];
    [searchview release];
    [searchbar release];
    [addjobbutton release];
    [enquirybutton release];
    [emailbutton release];
    
    [popupview release];
    [popupimage release];
    [popuplabel release];
    [popuppreviousbutton release];
    [popupnextbutton release];
    [popupclosebutton release];
    [loadingview release];
    
    [popcodelabel release];
    [popdescriptionlabel release];
    [jobtable release];
    
    [enquiryview release];
    [popfirstnametextfield release];
    [poplastnametextfield release];
    [popemailtextfield release];
    [popaddresstextfield release];
    [popcompanytextfield release];
    [popphonetextfield release];
    [popproductcodetextfield release];
    [popcommentstextview release];
    [popselectedbutton release];
    [popnotselectedbutton release];
    [popscrollview release];
    
    [emailcontroller release];
    [sendcontroller release];
    [addjobcontroller release];
    
    [db release];
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == categorytable)
    {
        return self.categoryArr.count;
    }
    else if(tableView == producttable)
    {
        return self.itemArr.count;
    }
    else if(tableView == jobtable)
    {
        return joblist.count;
    }
    else
    {
        return 1;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == categorytable)
    {
        NSDictionary* novainfo = [self.categoryArr objectAtIndex:indexPath.row];
        
        NSDictionary* novadetails = [novainfo objectForKey:@"category"];
        
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        categorycell= (CategoryCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(categorycell == nil){
            categorycell = [[[CategoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        categorycell.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.categorytable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        categorycell.title.text = [novadetails objectForKey:@"name"];
        
        CGSize maxsize = CGSizeMake(230, 9999);
        UIFont *thefont = [UIFont boldSystemFontOfSize:13];
        CGSize textsize = [categorycell.title.text sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
        
        categorycell.frame = CGRectMake(0, 0, MainTableWidth, textsize.height + 22);
        categorycell.separator.frame = CGRectMake(0, 2, MainTableWidth, categorycell.frame.size.height-2);
        categorycell.title.frame = CGRectMake(20, 12, 230, textsize.height);
        
        categorycell.thumbnailbutton.frame = CGRectMake(264, (categorycell.frame.size.height - 22) / 2 + 2, 26, 22);
        categorycell.thumbnailbutton.tag = indexPath.row;
        [categorycell.thumbnailbutton setBackgroundImage:[UIImage imageNamed:@"icon_grid.png"] forState:UIControlStateNormal];
        [categorycell.thumbnailbutton addTarget:self action:@selector(thumbnailview:) forControlEvents:UIControlEventTouchUpInside];
        
        [categorycell.separator setBackgroundColor:[UIColor colorWithRed:red / 255.0 green:green/255.0 blue:blue/255.0 alpha:1]];
        categorycell.gradient.frame = CGRectMake(254, 0, 46, categorycell.frame.size.height-2);
        
        if(indexPath.row == rowselected)
        {
            categorycell.title.textColor = [UIColor blackColor];
            [categorycell.separator setBackgroundColor:[UIColor whiteColor]];
        }
        else
        {
            categorycell.title.textColor = [UIColor whiteColor];
            [categorycell.separator setBackgroundColor:[UIColor colorWithRed:red / 255.0 green:green/255.0 blue:blue/255.0 alpha:1]];
        }
        
        return categorycell;
    }
    else if(tableView == producttable)
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        productcell= (ProductCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(productcell == nil)
        {
            productcell = [[[ProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        productcell.frame = CGRectMake(0, 0, MainTableWidth, ProductTableHeight);
        productcell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(itemArr != nil || [itemArr count] != 0)
        {
            NSError* error;
            
            NSArray* itemInfo = [totalItem objectAtIndex:indexPath.row];
            
            NSDictionary* singleItem = [itemInfo objectAtIndex:0];
            NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
            
            NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
            
            //Print the description of the product
            NSDictionary *itemDescription = [itemDetails objectForKey:@"98ac0b3a-035d-4367-97fb-033cd64f659a"];
            NSDictionary *item_ = [itemDescription objectForKey:@"0"];
            
            NSString *filterStr = [[item_ objectForKey:@"value"] stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
            
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<ul>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</ul>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<strong>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</strong>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<li>" withString:@"- "];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</li>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
            
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<i>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</i>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<u>" withString:@""];
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</u>" withString:@""];
            
            filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</span>" withString:@""];
            
            BOOL happen = true;
            
            do
            {
                NSRange strBegin = [filterStr rangeOfString:@"<span"];
                
                if(strBegin.location == NSNotFound)
                {
                    happen = false;
                }
                else
                {
                    NSRange strEnd = [filterStr rangeOfString:@"/>"];
                    
                    if(strEnd.location == NSNotFound)
                    {
                        //nothing here
                        happen = false;
                    }
                    else
                    {
                        //do the operation
                        filterStr = [filterStr substringWithRange:NSMakeRange(strBegin.location+1, strEnd.location-1)];
                    }
                }
            } while (happen);
            
            productcell.title.text = [singleDetails objectForKey:@"name"];
            
            productcell.description.text = filterStr;
            
            NSDictionary *itemDic = [itemDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
            NSDictionary *itemCode = [itemDic objectForKey:@"0"];
            
            productcell.code.text = [NSString stringWithFormat:@"Code : %@",[itemCode objectForKey:@"value"]];
            
            productcell.serial.text = [NSString stringWithFormat:@"%d", indexPath.row + 1];
            
            NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
            
            productcell.productimage.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
        }
        
        productcell.serialimage.image = [UIImage imageNamed:[NSString stringWithFormat:@"cat_tri_%d.png", self.row + 1]];
        
        if(indexPath.row == productrowselected)
        {
            productcell.title.textColor = [UIColor whiteColor];
            productcell.backgroundview.backgroundColor = [UIColor blackColor];
        }
        else
        {
            productcell.title.textColor = [UIColor blackColor];
            productcell.backgroundview.backgroundColor = [UIColor whiteColor];
        }
        
        return productcell;
    }
    else if(tableView == jobtable)
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        jobcell= (JobCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(jobcell == nil){
            jobcell = [[[JobCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        jobcell.frame = CGRectMake(0, 0, JobTableWidth, JobTableHeight);
        jobcell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        int myjobid = [db retrieveMyjobselectedrow:indexPath.row];
        joblist = [db retrieveMyjob];
        Myjob *myjob = [joblist objectAtIndex:indexPath.row];
        jobcell.jobtitle.text = [myjob.myjobtitle stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
        
        UIButton *addproductbutton = [[[UIButton alloc] initWithFrame:CGRectMake(JobTableWidth - 70, 10, 21, 21)] autorelease];
        addproductbutton.tag = indexPath.row;
        [addproductbutton setBackgroundImage:[UIImage imageNamed:@"btn_add.png"] forState:UIControlStateNormal];
        [jobcell addSubview:addproductbutton];
        [addproductbutton addTarget:self action:@selector(addproductinjob:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *deleteproductbutton = [[[UIButton alloc] initWithFrame:CGRectMake(JobTableWidth - 100, 10, 21, 21)] autorelease];
        deleteproductbutton.tag = indexPath.row;
        [deleteproductbutton setBackgroundImage:[UIImage imageNamed:@"btn_remove.png"] forState:UIControlStateNormal];
        [jobcell addSubview:deleteproductbutton];
        deleteproductbutton.enabled = NO;
        [deleteproductbutton addTarget:self action:@selector(removeproductinjob:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *quantitybutton = [[[UIButton alloc] initWithFrame:CGRectMake(JobTableWidth - 40, 9, 32, 23)] autorelease];
        quantitybutton.tag = indexPath.row;
        [quantitybutton setBackgroundImage:[UIImage imageNamed:@"btn_quantity.png"] forState:UIControlStateNormal];
        [jobcell addSubview:quantitybutton];
        [quantitybutton addTarget:self action:@selector(quantityadd:) forControlEvents:UIControlEventTouchUpInside];
        
        if([db countProductinjob:myjobid :[self.codelabel.text substringFromIndex:7]]> 0)
        {
            jobcell.jobdescription.text = [NSString stringWithFormat:@"Has %d of These", [db retrieveProductQuantity:myjobid :[self.codelabel.text substringFromIndex:7]]];
            
            if([db retrieveProductQuantity:myjobid :[self.codelabel.text substringFromIndex:7]] > 0)
            {
                deleteproductbutton.enabled = YES;
            }
        }
        else
        {
            deleteproductbutton.enabled = NO;
            jobcell.jobdescription.text = [NSString stringWithFormat:@" Has 0 of Those"];
        }
        
        return jobcell;
    }
    else
    {
        static NSString *hlCellID = @"hlCellID";
        
        UITableViewCell *hlcell = [tableView dequeueReusableCellWithIdentifier:hlCellID];
        if(hlcell == nil) {
            hlcell =  [[[UITableViewCell alloc]
                        initWithStyle:UITableViewCellStyleDefault reuseIdentifier:hlCellID] autorelease];
            hlcell.accessoryType = UITableViewCellAccessoryNone;
            hlcell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        UIView *existingToast = [hlcell.contentView viewWithTag:888];
        if (existingToast != nil) {
            [existingToast removeFromSuperview];
        }
        
        UIScrollView *scrollview = [[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 724, 748)] autorelease];
        scrollview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        scrollview.tag = 888;
        [hlcell.contentView addSubview:scrollview];
        int n = [self.itemArr count];
        int i=0, rows = 5, columns = 5;
        i1=0;
        
        int gapx = (724 - rows * 130) / (rows + 1);
        int gapy = (748 - columns * 140) / (columns + 1);
        
        while(i<n)
        {
            int yy = gapy + i1 * (140 + gapy);
            int j=0;
            for(j=0; j<columns;j++){
                if (i>=n) break;
                
                NSError* error;
                
                NSArray* itemInfo = [totalItem objectAtIndex:i];
                
                NSDictionary* singleItem = [itemInfo objectAtIndex:0];
                NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
                
                NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
                
                NSDictionary *itemDic = [itemDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
                NSDictionary *itemCode = [itemDic objectForKey:@"0"];
                
                NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
                
                UIView *thumbnailbackgroundview = [[[UIView alloc]initWithFrame:CGRectMake(gapx + j * (gapx + 130), yy, 130, 140)] autorelease];
                thumbnailbackgroundview.tag = i;
                thumbnailbackgroundview.backgroundColor = [UIColor whiteColor];
                UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(galleryview:)];
                [thumbnailbackgroundview addGestureRecognizer:singletap];
                [thumbnailbackgroundview setUserInteractionEnabled:YES];
                
                UIImageView *thumbnailview = [[UIImageView alloc]initWithFrame:CGRectMake(24, 0, 83, 104)];
                thumbnailview.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
                
                [thumbnailbackgroundview addSubview:thumbnailview];
                [thumbnailview release];
                
                UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(0, 104, 130, 36)];
                Title.textAlignment = UITextAlignmentCenter;
                Title.text = [itemCode objectForKey:@"value"];
                Title.textColor = [UIColor whiteColor];
                Title.backgroundColor = [UIColor blackColor];
                Title.font = [UIFont systemFontOfSize:14];
                Title.lineBreakMode = UILineBreakModeWordWrap;
                Title.numberOfLines = 0;
                [thumbnailbackgroundview addSubview:Title];
                [Title release];
                
                [scrollview addSubview:thumbnailbackgroundview];
                i++;
            }
            i1 = i1+1;
        }
        
        if(gapy + i1 * (140 + gapy) > 748)
            scrollview.contentSize = CGSizeMake(724, gapy + i1 * (140 + gapy));
        else
            scrollview.contentSize = CGSizeMake(724, 748);
        return hlcell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == categorytable)
    {
        [self.searchbar resignFirstResponder];
        [GlobalFunction AddLoadingScreen:self];
        self.thumbnailtable.hidden = YES;
        self.productview.hidden = YES;
        self.detailview.hidden = YES;
        categorycell= (CategoryCell*)[tableView cellForRowAtIndexPath:indexPath];
        rowselected = indexPath.row;
        [self performSelector:@selector(delay) withObject:nil afterDelay:0.1];
    }
    else if(tableView == producttable)
    {
        [GlobalFunction AddLoadingScreen:self];
        self.thumbnailtable.hidden = YES;
        self.detailview.hidden = YES;
        productcell= (ProductCell*)[tableView cellForRowAtIndexPath:indexPath];
        productrowselected = indexPath.row;
        [self performSelector:@selector(delay3) withObject:nil afterDelay:0.1];
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    if(tableView == categorytable)
    {
        categorycell= (CategoryCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return categorycell.frame.size.height;
    }
    else if(tableView == producttable)
    {
        productcell= (ProductCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return productcell.frame.size.height;
    }
    else if(tableView == jobtable)
    {
        jobcell= (JobCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return jobcell.frame.size.height;
    }
    else
    {
        return 748;
    }
}

-(void)delay
{
    NSError* error;
    
    self.productview.hidden = NO;
    self.thumbnailtable.hidden = YES;
    
    NSDictionary* novainfo = [self.categoryArr objectAtIndex:rowselected];
    
    NSDictionary* novadetails = [novainfo objectForKey:@"category"];
    
    WebServices *ws = [[WebServices alloc] init];
    
    itemArr = [[ws selectAllFromCategoryItemTableByID:[[novadetails objectForKey:@"id"] intValue]] retain];
    
    totalItem = [[[NSMutableArray alloc] initWithCapacity:itemArr.count+1] retain];
    
    //create an array... add object into it
    for(int i = 0; i <itemArr.count; i++)
    {
        NSDictionary* itemInfo = [itemArr objectAtIndex:i];
        NSDictionary* infoDetails = [itemInfo objectForKey:@"category_item"];
        
        NSArray *item = [ws selectItemFromItemTableByID:[[infoDetails objectForKey:@"item_id"] intValue]];
        
        [totalItem addObject:item];
    }
    
    //create an array for price & an array for middle image @@
    itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    middleImageArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    tradeArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    
    for(int j = 0; j < totalItem.count; j++)
    {
        NSArray* itemInfo = [totalItem objectAtIndex:j];
        
        NSDictionary* test = [itemInfo objectAtIndex:0];
        
        NSDictionary* infoDetails = [test objectForKey:@"item"];
        
        NSDictionary *elements = [NSJSONSerialization JSONObjectWithData: [[infoDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        //Print the price of the product
        NSDictionary *elementsKey = [elements objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
        NSDictionary *price = [elementsKey objectForKey:@"0"];
        
        NSDictionary *test11 = [elements objectForKey:@"cfca6d69-6dde-490f-a2ac-caa3b23921eb"];
        
        NSDictionary *tradeKey = [elements objectForKey:@"953ff3d0-753c-47f7-aeec-96599ecc4814"];
        NSDictionary *trade = [tradeKey objectForKey:@"0"];
        
        [middleImageArr addObject:[test11 objectForKey:@"file"]];
        
        [itemPriceArr addObject:[price objectForKey:@"value"]];
        
        [tradeArr addObject:[trade objectForKey:@"value"]];
    }
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    [self.producttable reloadData];
    
    productrowselected = -1;
    [self.categorytable reloadData];
}

-(void)delay2
{
    self.detailview.hidden = YES;
    self.productview.hidden = YES;
    
    NSError* error;
    
    NSDictionary* novainfo = [self.categoryArr objectAtIndex:rowselected];
    
    NSDictionary* novadetails = [novainfo objectForKey:@"category"];
    
    WebServices *ws = [[WebServices alloc] init];
    
    itemArr = [[ws selectAllFromCategoryItemTableByID:[[novadetails objectForKey:@"id"] intValue]] retain];
    
    totalItem = [[[NSMutableArray alloc] initWithCapacity:itemArr.count+1] retain];
    
    //create an array... add object into it
    for(int i = 0; i <itemArr.count; i++)
    {
        NSDictionary* itemInfo = [itemArr objectAtIndex:i];
        NSDictionary* infoDetails = [itemInfo objectForKey:@"category_item"];
        
        NSArray *item = [ws selectItemFromItemTableByID:[[infoDetails objectForKey:@"item_id"] intValue]];
        
        [totalItem addObject:item];
    }
    
    //create an array for price & an array for middle image @@
    itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    middleImageArr = [[[NSMutableArray alloc] initWithCapacity:totalItem.count+1] retain];
    
    for(int j = 0; j < totalItem.count; j++)
    {
        NSArray* itemInfo = [totalItem objectAtIndex:j];
        
        NSDictionary* test = [itemInfo objectAtIndex:0];
        
        NSDictionary* infoDetails = [test objectForKey:@"item"];
        
        NSDictionary *elements = [NSJSONSerialization JSONObjectWithData: [[infoDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        //Print the price of the product
        NSDictionary *elementsKey = [elements objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
        NSDictionary *price = [elementsKey objectForKey:@"0"];
        
        NSDictionary *test11 = [elements objectForKey:@"cfca6d69-6dde-490f-a2ac-caa3b23921eb"];
        
        [middleImageArr addObject:[test11 objectForKey:@"file"]];
        
        [itemPriceArr addObject:[price objectForKey:@"value"]];
    }
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    self.thumbnailtable.hidden = NO;
    [self.thumbnailtable reloadData];
    
    [self.categorytable reloadData];
}
-(void)delay3
{
    NSError* error;
    
    NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
    
    NSDictionary* singleItem = [itemInfo objectAtIndex:0];
    NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
    
    titleOfRowSelected = [singleDetails objectForKey:@"name"];
    
    NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    //Get imasge path - 1
    NSDictionary *galleryKey = [itemDetails objectForKey:@"5f304277-9eff-4033-88b9-e231db615697"];
    
    //Get image path - 2
    NSDictionary *technicalKey = [itemDetails objectForKey:@"35edcd61-decc-4ba2-9146-995bcbcb0245"];
    
    WebServices *ws = [[WebServices alloc] init];
    
    galleryImageArr = [[ws selectGalleryImageByPath:[galleryKey objectForKey:@"value"]] retain];
    technicalImageArr = [[ws selectTechnicalImageByPath:[technicalKey objectForKey:@"value"]] retain];
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    NSString *filterStr, *codeStr;
    
    //Print the description of the product
    NSDictionary *itemDescription = [itemDetails objectForKey:@"98ac0b3a-035d-4367-97fb-033cd64f659a"];
    NSDictionary *item_ = [itemDescription objectForKey:@"0"];
    
    NSDictionary *itemDic = [itemDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
    NSDictionary *itemCode = [itemDic objectForKey:@"0"];
    
    codeStr = [itemCode objectForKey:@"value"];
    
    filterStr = [[item_ objectForKey:@"value"] stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<ul>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</ul>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<strong>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</strong>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<li>" withString:@"- "];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</li>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<i>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</i>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<u>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</u>" withString:@""];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</span>" withString:@""];
    
    BOOL happen = true;
    
    do
    {
        NSRange strBegin = [filterStr rangeOfString:@"<span"];
        
        if(strBegin.location == NSNotFound)
        {
            happen = false;
        }
        else
        {
            NSRange strEnd = [filterStr rangeOfString:@"/>"];
            
            if(strEnd.location == NSNotFound)
            {
                //nothing here
                happen = false;
            }
            else
            {
                //do the operation
                filterStr = [filterStr substringWithRange:NSMakeRange(strBegin.location+1, strEnd.location-1)];
            }
        }
    } while (happen);
    
    self.detailview.hidden = NO;
    self.buttonbgimage.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    
    CGSize titlesize = CGSizeMake(394, 9999);
    UIFont *titlefont = [UIFont fontWithName:@"Helvetica" size:15];
    CGSize titletextsize = [[NSString stringWithFormat:@"%@", titleOfRowSelected] sizeWithFont:titlefont constrainedToSize:titlesize lineBreakMode:UILineBreakModeWordWrap];
    
    self.titlelabel.frame = CGRectMake(30, 242, 394, titletextsize.height + 15);
    self.titlebgimage.frame = CGRectMake(0, 242, 424, titletextsize.height + 15);
    if(titletextsize.height > 20)
        self.seriallabel.frame = CGRectMake(0, 242, 30, titletextsize.height / 2 + 15);
    else
        self.seriallabel.frame = CGRectMake(0, 242, 30, titletextsize.height + 15);
    self.detailscrollview.frame = CGRectMake(20, self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height + 10, 384, 670-252-self.titlelabel.frame.size.height);
    
    self.titlebgimage.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    
    self.titlelabel.text = [NSString stringWithFormat:@"%@", titleOfRowSelected];
    self.seriallabel.text = [NSString stringWithFormat:@"%d) ", productrowselected + 1];
    
    self.codelabel.text = [NSString stringWithFormat:@"Code : %@", codeStr];
    
    productCodeOfRowSelected = self.codelabel.text;
    
    NSDictionary *test = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    NSDictionary *test2 = [test objectForKey:@"695755a4-ae67-43bb-8c39-af4eb9ad4961"];
    NSDictionary *test3 = [test2 objectForKey:@"option"];
    
    NSDictionary *test5 = [[[NSDictionary alloc] init] autorelease];
    
    // trade price INCL GST part
    if([test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"] != nil)
    {
        NSDictionary *test4 = [test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"];
        
        test5 = [test4 objectForKey:@"option"];
    }
    
    if([tradeArr objectAtIndex:productrowselected] != nil && ![[tradeArr objectAtIndex:productrowselected] isEqualToString:@""])
    {
        if([test5 objectForKey:@"0"] != nil && ![[test5 objectForKey:@"0"] isEqualToString:@""])
            self.tradelabel.text = [NSString stringWithFormat:@"Trade : $%@ Incl GST", [tradeArr objectAtIndex:productrowselected]];
        else
            self.tradelabel.text = [NSString stringWithFormat:@"Trade : $%@ Excl GST", [tradeArr objectAtIndex:productrowselected]];
    }
    else
        self.tradelabel.text = @"Trade : -";
    
    if([itemPriceArr objectAtIndex:productrowselected] != nil && ![[itemPriceArr objectAtIndex:productrowselected] isEqualToString:@""])
    {
        if([test3 objectForKey:@"0"] != nil && ![[test3 objectForKey:@"0"] isEqualToString:@""])
            self.retaillabel.text = [NSString stringWithFormat:@"Retail : $%@ Incl GST", [itemPriceArr objectAtIndex:productrowselected]];
        else
            self.retaillabel.text = [NSString stringWithFormat:@"Retail : $%@ Excl GST", [itemPriceArr objectAtIndex:productrowselected]];
    }
    else
    {
        self.retaillabel.text = @"Retail : -";
    }
    
    imagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
    self.productimage.imageURL = [NSURL URLWithString:imagepathM];
    
    self.productimage.backgroundColor = [UIColor whiteColor];
    
    descriptionlabel.text = @"";
    CGSize maxsize = CGSizeMake(384, 9999);
    UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:17];
    CGSize textsize = [filterStr sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    descriptionlabel = [[[UILabel alloc]initWithFrame:CGRectMake(0, 85, 384, textsize.height)] autorelease];
    descriptionlabel.tag = 999;
    descriptionlabel.textColor = [UIColor blackColor];
    descriptionlabel.font = [UIFont fontWithName:@"Helvetica" size:17];
    descriptionlabel.text = filterStr;
    descriptionlabel.numberOfLines = 0;
    descriptionlabel.lineBreakMode = UILineBreakModeWordWrap;
    descriptionlabel.backgroundColor = [UIColor clearColor];
    [self.detailscrollview addSubview:descriptionlabel];
    self.detailscrollview.contentSize = CGSizeMake(384, 85 + descriptionlabel.frame.size.height);
    
    [self.producttable reloadData];
}

-(void)delay5
{
    NSError* error;
    
    NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
    
    NSDictionary* singleItem = [itemInfo objectAtIndex:0];
    NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
    
    titleOfRowSelected = [singleDetails objectForKey:@"name"];
    
    NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    //Get imasge path - 1
    NSDictionary *galleryKey = [itemDetails objectForKey:@"5f304277-9eff-4033-88b9-e231db615697"];
    
    //Get image path - 2
    NSDictionary *technicalKey = [itemDetails objectForKey:@"35edcd61-decc-4ba2-9146-995bcbcb0245"];
    
    WebServices *ws = [[WebServices alloc] init];
    
    galleryImageArr = [[ws selectGalleryImageByPath:[galleryKey objectForKey:@"value"]] retain];
    technicalImageArr = [[ws selectTechnicalImageByPath:[technicalKey objectForKey:@"value"]] retain];
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    NSString *filterStr, *codeStr;
    
    //Print the description of the product
    NSDictionary *itemDescription = [itemDetails objectForKey:@"98ac0b3a-035d-4367-97fb-033cd64f659a"];
    NSDictionary *item_ = [itemDescription objectForKey:@"0"];
    
    NSDictionary *itemDic = [itemDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
    NSDictionary *itemCode = [itemDic objectForKey:@"0"];
    
    codeStr = [itemCode objectForKey:@"value"];
    
    filterStr = [[item_ objectForKey:@"value"] stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<ul>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</ul>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<strong>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</strong>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<li>" withString:@"- "];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</li>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<i>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</i>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<u>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</u>" withString:@""];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</span>" withString:@""];
    
    BOOL happen = true;
    
    do
    {
        NSRange strBegin = [filterStr rangeOfString:@"<span"];
        
        if(strBegin.location == NSNotFound)
        {
            happen = false;
        }
        else
        {
            NSRange strEnd = [filterStr rangeOfString:@"/>"];
            
            if(strEnd.location == NSNotFound)
            {
                //nothing here
                happen = false;
            }
            else
            {
                //do the operation
                filterStr = [filterStr substringWithRange:NSMakeRange(strBegin.location+1, strEnd.location-1)];
            }
        }
    } while (happen);
    
    self.detailview.hidden = NO;
    self.buttonbgimage.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    
    CGSize titlesize = CGSizeMake(394, 9999);
    UIFont *titlefont = [UIFont fontWithName:@"Helvetica" size:15];
    CGSize titletextsize = [[NSString stringWithFormat:@"%@", titleOfRowSelected] sizeWithFont:titlefont constrainedToSize:titlesize lineBreakMode:UILineBreakModeWordWrap];
    
    self.titlelabel.frame = CGRectMake(30, 242, 394, titletextsize.height + 15);
    self.titlebgimage.frame = CGRectMake(0, 242, 424, titletextsize.height + 15);
    if(titletextsize.height > 20)
        self.seriallabel.frame = CGRectMake(0, 242, 30, titletextsize.height / 2 + 15);
    else
        self.seriallabel.frame = CGRectMake(0, 242, 30, titletextsize.height + 15);
    self.detailscrollview.frame = CGRectMake(20, self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height + 10, 384, 670-252-self.titlelabel.frame.size.height);
    
    self.titlebgimage.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    
    self.titlelabel.text = [NSString stringWithFormat:@"%@", titleOfRowSelected];
    self.seriallabel.text = [NSString stringWithFormat:@"%d) ", productrowselected + 1];
    
    self.codelabel.text = [NSString stringWithFormat:@"Code : %@", codeStr];
    
    productCodeOfRowSelected = self.codelabel.text;
    
    NSDictionary *test = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    NSDictionary *test2 = [test objectForKey:@"695755a4-ae67-43bb-8c39-af4eb9ad4961"];
    NSDictionary *test3 = [test2 objectForKey:@"option"];
    
    NSDictionary *test5 = [[[NSDictionary alloc] init] autorelease];
    
    // trade price INCL GST part
    if([test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"] != nil)
    {
        NSDictionary *test4 = [test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"];
        
        test5 = [test4 objectForKey:@"option"];
    }
    
    if([tradeArr objectAtIndex:productrowselected] != nil && ![[tradeArr objectAtIndex:productrowselected] isEqualToString:@""])
    {
        if([test5 objectForKey:@"0"] != nil && ![[test5 objectForKey:@"0"] isEqualToString:@""])
            self.tradelabel.text = [NSString stringWithFormat:@"Trade : $%@ Incl GST", [tradeArr objectAtIndex:productrowselected]];
        else
            self.tradelabel.text = [NSString stringWithFormat:@"Trade : $%@ Excl GST", [tradeArr objectAtIndex:productrowselected]];
    }
    else
        self.tradelabel.text = @"Trade : -";
    
    if([itemPriceArr objectAtIndex:productrowselected] != nil && ![[itemPriceArr objectAtIndex:productrowselected] isEqualToString:@""])
    {
        if([test3 objectForKey:@"0"] != nil && ![[test3 objectForKey:@"0"] isEqualToString:@""])
            self.retaillabel.text = [NSString stringWithFormat:@"Retail : $%@ Incl GST", [itemPriceArr objectAtIndex:productrowselected]];
        else
            self.retaillabel.text = [NSString stringWithFormat:@"Retail : $%@ Excl GST", [itemPriceArr objectAtIndex:productrowselected]];
    }
    else
    {
        self.retaillabel.text = @"Retail : -";
    }
    
    imagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
    self.productimage.imageURL = [NSURL URLWithString:imagepathM];
    
    self.productimage.backgroundColor = [UIColor whiteColor];
    
    descriptionlabel.text = @"";
    CGSize maxsize = CGSizeMake(384, 9999);
    UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:17];
    CGSize textsize = [filterStr sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    descriptionlabel = [[[UILabel alloc]initWithFrame:CGRectMake(0, 85, 384, textsize.height)] autorelease];
    descriptionlabel.tag = 999;
    descriptionlabel.textColor = [UIColor blackColor];
    descriptionlabel.font = [UIFont fontWithName:@"Helvetica" size:17];
    descriptionlabel.text = filterStr;
    descriptionlabel.numberOfLines = 0;
    descriptionlabel.lineBreakMode = UILineBreakModeWordWrap;
    descriptionlabel.backgroundColor = [UIColor clearColor];
    [self.detailscrollview addSubview:descriptionlabel];
    self.detailscrollview.contentSize = CGSizeMake(384, 85 + descriptionlabel.frame.size.height);
    
    [self.producttable reloadData];
    [self.categorytable reloadData];
    
    [self.producttable selectRowAtIndexPath:[NSIndexPath indexPathForRow:productrowselected inSection:self.producttable.indexPathForSelectedRow.section] animated:NO scrollPosition:UITableViewScrollPositionTop];
    [self.categorytable selectRowAtIndexPath:[NSIndexPath indexPathForRow:rowselected inSection:self.categorytable.indexPathForSelectedRow.section] animated:NO scrollPosition:UITableViewScrollPositionTop];
    
    self.categoryview.hidden = NO;
    self.productview.hidden = NO;
}

-(void)delay7
{
    NSError *error;
    
    WebServices *ws = [[WebServices alloc] init];
    
    //first, get the cat_id
    NSArray *categoryList2 = [ws selectCategoryIDByID:[itemID intValue]];
    NSString *categoryID = @"0";
    
    for(int i = 0; i < categoryList2.count; i++)
    {
        NSDictionary* catInfo2 = [categoryList2 objectAtIndex:i];
        NSDictionary* catDetails = [catInfo2 objectForKey:@"cat"];
        
        categoryID = [catDetails objectForKey:@"category_id"];
    }
    
    NSArray *parentKeyArr = [ws getParentKeyByItemID:categoryID];
    
    for(int i = 0; i < parentKeyArr.count; i++)
    {
        NSDictionary* novainfo = [parentKeyArr objectAtIndex:i];
        
        NSDictionary *novadetails = [novainfo objectForKey:@"parentid"];
        
        appID = [NSString stringWithFormat:@"%@", [novadetails objectForKey:@"parent"]];
    }
    
    NSArray *mainArr = [[ws selectAllFromApplicationTableExceptParam] retain];
    
    for(int i = 0; i < mainArr.count; i++)
    {
        NSDictionary* novainfo = [mainArr objectAtIndex:i];
        NSDictionary* novadetails = [novainfo objectForKey:@"application"];
        
        if([[novadetails objectForKey:@"id"]isEqualToString:self.appID])
        {
            row = i;
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            red = [[appDelegate.colorcodered objectAtIndex:row] floatValue];
            green = [[appDelegate.colorcodegreen objectAtIndex:row] floatValue];
            blue = [[appDelegate.colorcodeblue objectAtIndex:row] floatValue];
            
            self.categoryheaderbg.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
            
            CAGradientLayer *gradient = [CAGradientLayer layer];
            gradient.frame = self.categoryheaderbg.frame;
            gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1] CGColor], [appDelegate.gradientbottom objectAtIndex:row], nil];
            [self.categoryheaderbg.layer insertSublayer:gradient atIndex:0];
        }
    }
    
    NSArray *appName = [ws selectApplicationNameByID:[appID intValue]];
    NSDictionary* appInfo = [appName objectAtIndex:0];
    NSDictionary* appDetails = [appInfo objectForKey:@"app"];
    
    self.categorylabel.text = [appDetails objectForKey:@"name"];
    
    NSArray *categoryList = [ws selectAllFromCategoryTableExceptParam:[appID intValue]];
    
    for(int i = 0; i < categoryList.count; i++)
    {
        NSDictionary* catInfo = [categoryList objectAtIndex:i];
        
        NSDictionary* catDetails = [catInfo objectForKey:@"category"];
        
        if([[catDetails objectForKey:@"id"] intValue] == [categoryID intValue])
        {
            rowselected = i;
        }
    }
    
    self.categoryArr = [categoryList mutableCopy];
    beforesearch = self.categoryArr;
    
    NSArray *itemList = [ws selectAllCategoryItem:[itemID intValue]];
    self.itemArr = itemList;
    self.totalItem = [[[NSMutableArray alloc] initWithCapacity:self.itemArr.count+1] retain];
    
    for(int i = 0; i < itemList.count; i++)
    {
        NSDictionary* itemInfo = [itemList objectAtIndex:i];
        NSDictionary* infoDetails = [itemInfo objectForKey:@"item"];
        
        NSArray *item = [ws selectItemFromItemTableByID:[[infoDetails objectForKey:@"item_id"] intValue]];
        
        [self.totalItem addObject:item];
        
        if([[infoDetails objectForKey:@"item_id"] intValue] == [itemID intValue])
        {
            productrowselected = i;
        }
    }
    
    self.itemPriceArr = [[[NSMutableArray alloc] initWithCapacity:self.totalItem.count+1] retain];
    self.middleImageArr = [[[NSMutableArray alloc] initWithCapacity:self.totalItem.count+1] retain];
    self.tradeArr = [[[NSMutableArray alloc] initWithCapacity:self.totalItem.count+1] retain];
    
    for(int j = 0; j < self.totalItem.count; j++)
    {
        NSArray* itemInfo = [self.totalItem objectAtIndex:j];
        
        NSDictionary* items = [itemInfo objectAtIndex:0];
        NSDictionary* infoDetails = [items objectForKey:@"item"];
        NSDictionary *elements = [NSJSONSerialization JSONObjectWithData: [[infoDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        //Print the price of the product
        NSDictionary *elementsKey = [elements objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
        NSDictionary *price = [elementsKey objectForKey:@"0"];
        
        NSDictionary *middleimg = [elements objectForKey:@"cfca6d69-6dde-490f-a2ac-caa3b23921eb"];
        
        NSDictionary *tradeKey = [elements objectForKey:@"953ff3d0-753c-47f7-aeec-96599ecc4814"];
        NSDictionary *trade = [tradeKey objectForKey:@"0"];
        
        [self.middleImageArr addObject:[middleimg objectForKey:@"file"]];
        [self.itemPriceArr addObject:[price objectForKey:@"value"]];
        [self.tradeArr addObject:[trade objectForKey:@"value"]];
    }
    
    NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
    
    NSDictionary* singleItem = [itemInfo objectAtIndex:0];
    NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
    
    titleOfRowSelected = [singleDetails objectForKey:@"name"];
    
    NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    //Get imasge path - 1
    NSDictionary *galleryKey = [itemDetails objectForKey:@"5f304277-9eff-4033-88b9-e231db615697"];
    
    //Get image path - 2
    NSDictionary *technicalKey = [itemDetails objectForKey:@"35edcd61-decc-4ba2-9146-995bcbcb0245"];
    
    galleryImageArr = [[ws selectGalleryImageByPath:[galleryKey objectForKey:@"value"]] retain];
    technicalImageArr = [[ws selectTechnicalImageByPath:[technicalKey objectForKey:@"value"]] retain];
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    NSString *filterStr, *codeStr;
    
    //Print the description of the product
    NSDictionary *itemDescription = [itemDetails objectForKey:@"98ac0b3a-035d-4367-97fb-033cd64f659a"];
    NSDictionary *item_ = [itemDescription objectForKey:@"0"];
    
    NSDictionary *itemDic = [itemDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
    NSDictionary *itemCode = [itemDic objectForKey:@"0"];
    
    codeStr = [itemCode objectForKey:@"value"];
    
    filterStr = [[item_ objectForKey:@"value"] stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<ul>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</ul>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<strong>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</strong>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<li>" withString:@"- "];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</li>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<i>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</i>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"<u>" withString:@""];
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</u>" withString:@""];
    
    filterStr = [filterStr stringByReplacingOccurrencesOfString:@"</span>" withString:@""];
    
    BOOL happen = true;
    
    do
    {
        NSRange strBegin = [filterStr rangeOfString:@"<span"];
        
        if(strBegin.location == NSNotFound)
        {
            happen = false;
        }
        else
        {
            NSRange strEnd = [filterStr rangeOfString:@"/>"];
            
            if(strEnd.location == NSNotFound)
            {
                //nothing here
                happen = false;
            }
            else
            {
                //do the operation
                filterStr = [filterStr substringWithRange:NSMakeRange(strBegin.location+1, strEnd.location-1)];
            }
        }
    } while (happen);
    
    self.detailview.hidden = NO;
    self.buttonbgimage.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
    
    CGSize titlesize = CGSizeMake(394, 9999);
    UIFont *titlefont = [UIFont fontWithName:@"Helvetica" size:15];
    CGSize titletextsize = [[NSString stringWithFormat:@"%@", titleOfRowSelected] sizeWithFont:titlefont constrainedToSize:titlesize lineBreakMode:UILineBreakModeWordWrap];
    
    self.titlelabel.frame = CGRectMake(30, 242, 394, titletextsize.height + 15);
    self.titlebgimage.frame = CGRectMake(0, 242, 424, titletextsize.height + 15);
    if(titletextsize.height > 20)
        self.seriallabel.frame = CGRectMake(0, 242, 30, titletextsize.height / 2 + 15);
    else
        self.seriallabel.frame = CGRectMake(0, 242, 30, titletextsize.height + 15);
    self.detailscrollview.frame = CGRectMake(20, self.titlelabel.frame.origin.y + self.titlelabel.frame.size.height + 10, 384, 670-252-self.titlelabel.frame.size.height);
    
    self.titlebgimage.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    
    self.titlelabel.text = [NSString stringWithFormat:@"%@", titleOfRowSelected];
    self.seriallabel.text = [NSString stringWithFormat:@"%d) ", productrowselected + 1];
    
    self.codelabel.text = [NSString stringWithFormat:@"Code : %@", codeStr];
    
    productCodeOfRowSelected = self.codelabel.text;
    
    NSDictionary *test = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    NSDictionary *test2 = [test objectForKey:@"695755a4-ae67-43bb-8c39-af4eb9ad4961"];
    NSDictionary *test3 = [test2 objectForKey:@"option"];
    
    NSDictionary *test5 = [[[NSDictionary alloc] init] autorelease];
    
    // trade price INCL GST part
    if([test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"] != nil)
    {
        NSDictionary *test4 = [test objectForKey:@"581b36e3-59c1-458a-bdc4-8016209731e6"];
        
        test5 = [test4 objectForKey:@"option"];
    }
    
    if([tradeArr objectAtIndex:productrowselected] != nil && ![[tradeArr objectAtIndex:productrowselected] isEqualToString:@""])
    {
        if([test5 objectForKey:@"0"] != nil && ![[test5 objectForKey:@"0"] isEqualToString:@""])
            self.tradelabel.text = [NSString stringWithFormat:@"Trade : $%@ Incl GST", [tradeArr objectAtIndex:productrowselected]];
        else
            self.tradelabel.text = [NSString stringWithFormat:@"Trade : $%@ Excl GST", [tradeArr objectAtIndex:productrowselected]];
    }
    else
        self.tradelabel.text = @"Trade : -";
    
    if([itemPriceArr objectAtIndex:productrowselected] != nil && ![[itemPriceArr objectAtIndex:productrowselected] isEqualToString:@""])
    {
        if([test3 objectForKey:@"0"] != nil && ![[test3 objectForKey:@"0"] isEqualToString:@""])
            self.retaillabel.text = [NSString stringWithFormat:@"Retail : $%@ Incl GST", [itemPriceArr objectAtIndex:productrowselected]];
        else
            self.retaillabel.text = [NSString stringWithFormat:@"Retail : $%@ Excl GST", [itemPriceArr objectAtIndex:productrowselected]];
    }
    else
    {
        self.retaillabel.text = @"Retail : -";
    }
    
    imagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
    self.productimage.imageURL = [NSURL URLWithString:imagepathM];
    
    self.productimage.backgroundColor = [UIColor whiteColor];
    
    descriptionlabel.text = @"";
    CGSize maxsize = CGSizeMake(384, 9999);
    UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:17];
    CGSize textsize = [filterStr sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    descriptionlabel = [[[UILabel alloc]initWithFrame:CGRectMake(0, 85, 384, textsize.height)] autorelease];
    descriptionlabel.tag = 999;
    descriptionlabel.textColor = [UIColor blackColor];
    descriptionlabel.font = [UIFont fontWithName:@"Helvetica" size:17];
    descriptionlabel.text = filterStr;
    descriptionlabel.numberOfLines = 0;
    descriptionlabel.lineBreakMode = UILineBreakModeWordWrap;
    descriptionlabel.backgroundColor = [UIColor clearColor];
    [self.detailscrollview addSubview:descriptionlabel];
    self.detailscrollview.contentSize = CGSizeMake(384, 85 + descriptionlabel.frame.size.height);
    
    [self.producttable reloadData];
    [self.categorytable reloadData];
    
    [self.producttable selectRowAtIndexPath:[NSIndexPath indexPathForRow:productrowselected inSection:self.producttable.indexPathForSelectedRow.section] animated:NO scrollPosition:UITableViewScrollPositionTop];
    [self.categorytable selectRowAtIndexPath:[NSIndexPath indexPathForRow:rowselected inSection:self.categorytable.indexPathForSelectedRow.section] animated:NO scrollPosition:UITableViewScrollPositionTop];
    
    self.categoryview.hidden = NO;
    self.productview.hidden = NO;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController*)popoverController
{
    addjobpop = nil;
    sendpop = nil;
    emailpop = nil;
}

-(void) singletapcaptured:(UIGestureRecognizer *)gesture
{
    CGPoint tapLocation = [gesture locationInView: self.popupview];
    for (UIImageView *imageview in self.popupview.subviews)
    {
        if (CGRectContainsPoint(imageview.frame, tapLocation))
        {
            break;
        }
        else
        {
            [checkimagetimer invalidate];
            [self.popupview removeFromSuperview];
            self.popupnextbutton.hidden = YES;
            self.popuppreviousbutton.hidden = YES;
            self.popupclosebutton.hidden = YES;
            self.popupimage.imageURL = nil;
            break;
        }
    }
}

-(IBAction)thumbnailview:(id)sender
{
    self.productview.hidden = YES;
    self.detailview.hidden = YES;
    [GlobalFunction AddLoadingScreen:self];
    rowselected = [sender tag];
    [self performSelector:@selector(delay2) withObject:nil afterDelay:0.1];
}

-(void)galleryview:(UIGestureRecognizer *)gesture
{
    [GlobalFunction AddLoadingScreen:self];
    productrowselected = [gesture.view tag];
    [self performSelector:@selector(delay5) withObject:nil afterDelay:0.1];
}

-(IBAction)back:(id)sender
{
    [super dismissModalViewControllerAnimated:YES];
}

-(IBAction)search:(id)sender
{
    self.searchview.backgroundColor = [UIColor colorWithRed:red/ 255.0 green:green /255.0 blue:blue/255.0 alpha:1];
    self.categorytable.tableHeaderView = self.searchview;
    [self.categorytable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

-(IBAction)cancelsearch:(id)sender
{
    self.categorytable.tableHeaderView = transparentview;
    [self resetSearch];
}

-(IBAction)zoomin:(id)sender
{
    [self.popupview setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singletapcaptured:)];
    [self.popupview addGestureRecognizer:singletap];
    [self.popupview setUserInteractionEnabled:YES];
    istechnical = NO;
    
    if(galleryImageArr.count > 0)
    {
        self.popupimage.imageURL = nil;
        NSDictionary* galleryImage = [galleryImageArr objectAtIndex:0];
        
        NSString *imageUrl = [[galleryImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
        inviewpage = 0;
        self.popuplabel.text = titleOfRowSelected;
        checkimagetimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkimage) userInfo:nil repeats:YES];
        [self.view addSubview:self.popupview];
    }
    else
    {
        [self.view makeToast:@"No images to view！" duration:(0.3) position:@"center"];
    }
}

-(IBAction)drawing:(id)sender
{
    [self.popupview setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singletapcaptured:)];
    [self.popupview addGestureRecognizer:singletap];
    [self.popupview setUserInteractionEnabled:YES];
    istechnical = YES;
    
    if(technicalImageArr.count > 0)
    {
        self.popupimage.imageURL = nil;
        NSDictionary* technicalImage = [technicalImageArr objectAtIndex:0];
        NSString *imageUrl = [[technicalImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
        inviewpage = 0;
        self.popuplabel.text = titleOfRowSelected;
        checkimagetimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkimage) userInfo:nil repeats:YES];
        [self.view addSubview:self.popupview];
    }
    else
    {
        [self.view makeToast:@"No drawing to view！" duration:(0.3) position:@"center"];
    }
}

-(IBAction)bookmarks:(id)sender
{
    NSError* error;
    
    NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
    
    NSDictionary* singleItem = [itemInfo objectAtIndex:0];
    NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
    
    NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
    
    NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
    
    Bookmark *bookmark = [[Bookmark alloc] init];
    bookmark.bookmarktitle = self.titlelabel.text;
    bookmark.bookmarkcode = [self.codelabel.text substringFromIndex:7];
    bookmark.bookmarktrade = [self.tradelabel.text substringFromIndex:8];
    bookmark.bookmarkretail = [self.retaillabel.text substringFromIndex:9];
    descriptionlabel.text = [descriptionlabel.text stringByReplacingOccurrencesOfString:@"'" withString:@""];
    bookmark.bookmarkdescription = descriptionlabel.text;
    
    
    bookmark.bookmarkimagepathS = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]];
    bookmark.bookmarkimagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
    
    
    bookmark.sectionid = [[singleDetails objectForKey:@"application_id"] intValue];
    bookmark.categoryid = [[singleDetails objectForKey:@"id"] intValue];
    
    NSLog(@"%d , %d", [[singleDetails objectForKey:@"application_id"] intValue], [[singleDetails objectForKey:@"id"] intValue]);
    [db insertBookmark:bookmark];
    [bookmark release];
    
    alert = [[UIAlertView alloc] initWithTitle:nil message:@"Successfully added product into bookmark！" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

-(IBAction)add:(id)sender
{
    self.popcodelabel.text = [self.codelabel.text substringFromIndex:7];
    joblist = [db retrieveMyjob];
    [self.jobtable reloadData];
    
    addjobpop = [[UIPopoverController alloc] initWithContentViewController:self.addjobcontroller];
    addjobpop.delegate = self;
    [addjobpop presentPopoverFromRect:self.addjobbutton.frame inView:self.detailview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

-(IBAction)enquiry:(id)sender
{
    //add item code
    popproductcodetextfield.text = [productCodeOfRowSelected substringFromIndex:7];
    
    [self.enquiryview setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    
    //[];
    [self.view addSubview:self.enquiryview];
}

-(IBAction)email:(id)sender
{
    emailpop = [[UIPopoverController alloc] initWithContentViewController:self.emailcontroller];
    emailpop.delegate = self;
    [emailpop presentPopoverFromRect:self.emailbutton.frame inView:self.detailview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

//popup//
-(void)checkimage
{
    if(self.popupimage.image == nil)
    {
        self.popupnextbutton.hidden = YES;
        self.popuppreviousbutton.hidden = YES;
        self.popupclosebutton.hidden = YES;
        self.loadingview.hidden = NO;
        [self.loadingview startAnimating];
    }
    else
    {
        float width = self.popupimage.image.size.width * (float)600 / self.popupimage.image.size.height;
        if(width < [[UIScreen mainScreen] bounds].size.height)
        {
            self.popupimage.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.height - width) / 2, 59, width, 600);
            
            self.popuplabel.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.height - width) / 2, self.popupimage.frame.size.height + self.popupimage.frame.origin.y, width, 30);
            
            self.popupclosebutton.frame = CGRectMake(self.popupimage.frame.size.width + self.popupimage.frame.origin.x - 67, self.popupimage.frame.origin.y, 67, 67);
            
            self.popupnextbutton.frame = CGRectMake(self.popupimage.frame.size.width + self.popupimage.frame.origin.x - 67, self.popupimage.frame.origin.y + ((self.popupimage.frame.size.height - 67) / 2), 67, 67);
            
            self.popuppreviousbutton.frame = CGRectMake(self.popupimage.frame.origin.x, self.popupimage.frame.origin.y + ((self.popupimage.frame.size.height - 67) / 2), 67, 67);
        }
        else
        {
            float height = self.popupimage.image.size.height * (float)650 / self.popupimage.image.size.width;
            
            self.popupimage.frame = CGRectMake(187, ([[UIScreen mainScreen] bounds].size.width - height) / 2, 650, height);
            
            self.popuplabel.frame = CGRectMake(self.popupimage.frame.origin.x, self.popupimage.frame.size.height + self.popupimage.frame.origin.y, 650, 30);
            
            self.popupclosebutton.frame = CGRectMake(self.popupimage.frame.size.width + self.popupimage.frame.origin.x - 67, self.popupimage.frame.origin.y, 67, 67);
            
            self.popupnextbutton.frame = CGRectMake(self.popupimage.frame.size.width + self.popupimage.frame.origin.x - 67, self.popupimage.frame.origin.y + ((self.popupimage.frame.size.height - 67) / 2), 67, 67);
            
            self.popuppreviousbutton.frame = CGRectMake(self.popupimage.frame.origin.x, self.popupimage.frame.origin.y + ((self.popupimage.frame.size.height - 67) / 2), 67, 67);
        }
    }
    
    if(istechnical)
    {
        if(self.technicalImageArr.count <= 1 && self.popupimage.image != nil)
        {
            self.popupnextbutton.hidden = YES;
            self.popuppreviousbutton.hidden = YES;
            self.popupclosebutton.hidden = NO;
            self.loadingview.hidden = YES;
        }
        else
        {
            if(inviewpage <= 0 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = NO;
                self.popuppreviousbutton.hidden = YES;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
            else if(inviewpage >= self.technicalImageArr.count -1 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = YES;
                self.popuppreviousbutton.hidden = NO;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
            else if(inviewpage >= self.technicalImageArr.count -1 && inviewpage <= 0 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = NO;
                self.popuppreviousbutton.hidden = NO;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
        }
        
    }
    else
    {
        if(self.galleryImageArr.count <= 1 && self.popupimage.image != nil)
        {
            self.popupnextbutton.hidden = YES;
            self.popuppreviousbutton.hidden = YES;
            self.popupclosebutton.hidden = NO;
            self.loadingview.hidden = YES;
        }
        else
        {
            if(inviewpage <= 0 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = NO;
                self.popuppreviousbutton.hidden = YES;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
            else if(inviewpage >= self.galleryImageArr.count -1 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = YES;
                self.popuppreviousbutton.hidden = NO;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
            else if(inviewpage >= self.galleryImageArr.count -1 && inviewpage <= 0 && self.popupimage.image != nil)
            {
                self.popupnextbutton.hidden = NO;
                self.popuppreviousbutton.hidden = NO;
                self.popupclosebutton.hidden = NO;
                self.loadingview.hidden = YES;
            }
        }
    }
}

-(IBAction)closepopup:(id)sender
{
    [self.popupview removeFromSuperview];
    self.popupnextbutton.hidden = YES;
    self.popuppreviousbutton.hidden = YES;
    self.popupclosebutton.hidden = YES;
    self.popupimage.imageURL = nil;
    [checkimagetimer invalidate];
}

-(IBAction)next:(id)sender
{
    self.popupimage.imageURL = nil;
    if(istechnical)
    {
        inviewpage++;
        
        NSDictionary* technicalImage = [technicalImageArr objectAtIndex:inviewpage];
        NSString *imageUrl = [[technicalImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
    }
    else
    {
        inviewpage++;
        
        NSDictionary* technicalImage = [galleryImageArr objectAtIndex:inviewpage];
        NSString *imageUrl = [[technicalImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
    }
}

-(IBAction)previous:(id)sender
{
    self.popupimage.imageURL = nil;
    if(istechnical)
    {
        inviewpage--;
        NSDictionary* technicalImage = [technicalImageArr objectAtIndex:inviewpage];
        NSString *imageUrl = [[technicalImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
    }
    else
    {
        inviewpage--;
        NSDictionary* technicalImage = [galleryImageArr objectAtIndex:inviewpage];
        NSString *imageUrl = [[technicalImage objectForKey:@"img"] stringByReplacingOccurrencesOfString:@"../" withString:@"http://crh-app.novafusion.net:8052/"];
        imageUrl = [imageUrl stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        self.popupimage.imageURL = [NSURL URLWithString:imageUrl];
    }
}
/////////

//addjob//
-(IBAction)addproductinjob:(id)sender
{
    buttontag = [db retrieveMyjobselectedrow:[sender tag]];
    
    if([db countProductinjob:buttontag :[self.codelabel.text substringFromIndex:7]]> 0)
    {
        int quantity = [db retrieveProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7]];
        [db updateProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7] :quantity+1];
        /*isadd = YES;
         isexist = YES;
         alertaddjob = [[UIAlertView alloc] initWithTitle:@"Product quantity to be add" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
         quantitytextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)] autorelease];
         quantitytextfield.delegate = self;
         quantitytextfield.keyboardType = UIKeyboardTypeNamePhonePad;
         [quantitytextfield becomeFirstResponder];
         [quantitytextfield setBackgroundColor:[UIColor whiteColor]];
         quantitytextfield.text = @"1";
         quantitytextfield.textAlignment=UITextAlignmentCenter;
         quantitytextfield.layer.cornerRadius=5.0;
         [alertaddjob addSubview:quantitytextfield];
         [alertaddjob show];
         [alertaddjob release];*/
    }
    else
    {
        NSError* error;
        
        NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
        
        NSDictionary* singleItem = [itemInfo objectAtIndex:0];
        NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
        
        NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
        
        NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
        
        Product *product = [[Product alloc] init];
        product.producttitle = self.titlelabel.text;
        product.productcode = [self.codelabel.text substringFromIndex:7];;
        product.producttrade = [self.tradelabel.text substringFromIndex:8];
        product.productretail = [self.retaillabel.text substringFromIndex:9];
        product.productdescription = [descriptionlabel.text stringByReplacingOccurrencesOfString:@"'" withString:@""];
        product.productquantity = 1;
        
        product.productimagepathS = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]];
        product.productimagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
        
        product.myjobid = buttontag;
        product.sectionid = [[singleDetails objectForKey:@"application_id"] intValue];
        product.categoryid = [[singleDetails objectForKey:@"id"] intValue];
        product.myjobid = buttontag;
        [db insertProduct:product];
        [product release];
        /*isadd = YES;
         isexist = NO;
         alertaddjob = [[UIAlertView alloc] initWithTitle:@"Product quantity to be add" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
         quantitytextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)] autorelease];
         quantitytextfield.delegate = self;
         quantitytextfield.keyboardType = UIKeyboardTypeNamePhonePad;
         [quantitytextfield becomeFirstResponder];
         [quantitytextfield setBackgroundColor:[UIColor whiteColor]];
         quantitytextfield.text = @"1";
         quantitytextfield.textAlignment=UITextAlignmentCenter;
         quantitytextfield.layer.cornerRadius=5.0;
         [alertaddjob addSubview:quantitytextfield];
         [alertaddjob show];
         [alertaddjob release];*/
    }
    joblist = [db retrieveMyjob];
    [self.jobtable reloadData];
}

-(IBAction)removeproductinjob:(id)sender
{
    isadd = NO;
    buttontag = [db retrieveMyjobselectedrow:[sender tag]];
    int quantity = [db retrieveProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7]];
    [db updateProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7] :quantity-1];
    joblist = [db retrieveMyjob];
    [self.jobtable reloadData];
    /*alertaddjob = [[UIAlertView alloc] initWithTitle:@"Product quantity to be reduce" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Reduce", nil];
     quantitytextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)] autorelease];
     quantitytextfield.delegate = self;
     quantitytextfield.keyboardType = UIKeyboardTypeNamePhonePad;
     [quantitytextfield becomeFirstResponder];
     [quantitytextfield setBackgroundColor:[UIColor whiteColor]];
     quantitytextfield.text = [NSString stringWithFormat:@"%d", quantity];
     quantitytextfield.textAlignment=UITextAlignmentCenter;
     quantitytextfield.layer.cornerRadius=5.0;
     [alertaddjob addSubview:quantitytextfield];
     [alertaddjob show];
     [alertaddjob release];*/
}

-(IBAction)quantityadd:(id)sender
{
    buttontag = [db retrieveMyjobselectedrow:[sender tag]];
    if([db countProductinjob:buttontag :[self.codelabel.text substringFromIndex:7]]> 0)
    {
        isadd = YES;
        isexist = YES;
        alertaddjob = [[UIAlertView alloc] initWithTitle:@"Product quantity to be add" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
        quantitytextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)] autorelease];
        quantitytextfield.delegate = self;
        quantitytextfield.keyboardType = UIKeyboardTypeNamePhonePad;
        [quantitytextfield becomeFirstResponder];
        [quantitytextfield setBackgroundColor:[UIColor whiteColor]];
        quantitytextfield.text = @"1";
        quantitytextfield.textAlignment=UITextAlignmentCenter;
        quantitytextfield.layer.cornerRadius=5.0;
        [alertaddjob addSubview:quantitytextfield];
        [alertaddjob show];
        [alertaddjob release];
    }
    else
    {
        isadd = YES;
        isexist = NO;
        alertaddjob = [[UIAlertView alloc] initWithTitle:@"Product quantity to be add" message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
        quantitytextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)] autorelease];
        quantitytextfield.delegate = self;
        quantitytextfield.keyboardType = UIKeyboardTypeNamePhonePad;
        [quantitytextfield becomeFirstResponder];
        [quantitytextfield setBackgroundColor:[UIColor whiteColor]];
        quantitytextfield.text = @"1";
        quantitytextfield.textAlignment=UITextAlignmentCenter;
        quantitytextfield.layer.cornerRadius=5.0;
        [alertaddjob addSubview:quantitytextfield];
        [alertaddjob show];
        [alertaddjob release];
    }
}

-(IBAction)newjob:(id)sender
{
    alert = [[UIAlertView alloc] initWithTitle:@"\n" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
    newjobtitletextfield = [[[UITextField alloc] initWithFrame:CGRectMake(12.0, 15.0, 260.0, 25.0)] autorelease];
    newjobtitletextfield.delegate = self;
    [newjobtitletextfield becomeFirstResponder];
    [newjobtitletextfield setBackgroundColor:[UIColor whiteColor]];
    newjobtitletextfield.textAlignment=UITextAlignmentCenter;
    newjobtitletextfield.layer.cornerRadius=5.0;
    [alert addSubview:newjobtitletextfield];
    [alert show];
    [alert release];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView == alertenquiry)
    {
        [self.enquiryview removeFromSuperview];
    }
    else
    {
        if(buttonIndex == 0)
        {
            [alertView dismissWithClickedButtonIndex:0 animated:NO];
        }
        else if(alertView ==alertaddjob)
        {
            if(buttonIndex != 0)
            {
                BOOL valid;
                NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
                NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:quantitytextfield.text];
                valid = [alphaNums isSupersetOfSet:inStringSet];
                if (valid)
                {
                    if(isadd)
                    {
                        if(isexist)
                        {
                            int quantity = [db retrieveProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7]];
                            
                            [db updateProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7] :[quantitytextfield.text intValue] + quantity];
                        }
                        else
                        {
                            NSError* error;
                            
                            NSArray* itemInfo = [totalItem objectAtIndex:productrowselected];
                            
                            NSDictionary* singleItem = [itemInfo objectAtIndex:0];
                            NSDictionary* singleDetails = [singleItem objectForKey:@"item"];
                            
                            NSDictionary *itemDetails = [NSJSONSerialization JSONObjectWithData: [[singleDetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
                            
                            NSDictionary *thumbpath = [itemDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
                            
                            Product *product = [[Product alloc] init];
                            product.producttitle = self.titlelabel.text;
                            product.productcode = [self.codelabel.text substringFromIndex:7];;
                            product.producttrade = [self.tradelabel.text substringFromIndex:8];
                            product.productretail = [self.retaillabel.text substringFromIndex:9];
                            product.productdescription = [descriptionlabel.text stringByReplacingOccurrencesOfString:@"'" withString:@""];
                            product.productquantity = [quantitytextfield.text intValue];
                            
                            
                            product.productimagepathS = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]];
                            product.productimagepathM = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
                            
                            
                            product.myjobid = buttontag;
                            product.sectionid = [[singleDetails objectForKey:@"application_id"] intValue];
                            product.categoryid = [[singleDetails objectForKey:@"id"] intValue];
                            product.myjobid = buttontag;
                            [db insertProduct:product];
                            [product release];
                        }
                        joblist = [db retrieveMyjob];
                        [self.jobtable reloadData];
                        alert = [[UIAlertView alloc] initWithTitle:nil message:@"Product quantity has been increased！" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        [alert release];
                    }
                    else
                    {
                        int quantity = [db retrieveProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7]];
                        if([quantitytextfield.text intValue] > quantity)
                        {
                            [self.view makeToast:@"Cannot remove more than in database!" duration:(0.3) position:@"center"];
                            [quantitytextfield becomeFirstResponder];
                            [alertaddjob show];
                        }
                        else
                        {
                            if(quantity - [quantitytextfield.text intValue] == 0)
                            {
                                [db deleteProductWithCode:[self.codelabel.text substringFromIndex:7] :buttontag];
                            }
                            else
                            {
                                [db updateProductQuantity:buttontag :[self.codelabel.text substringFromIndex:7] :quantity -[quantitytextfield.text intValue]];
                            }
                            joblist = [db retrieveMyjob];
                            [self.jobtable reloadData];
                            alert = [[UIAlertView alloc] initWithTitle:nil message:@"Product quantity has been reduced！" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [alert show];
                            [alert release];
                        }
                    }
                }
                else
                {
                    [self.view makeToast:@"Insert only numeric!" duration:(0.3) position:@"center"];
                    [quantitytextfield becomeFirstResponder];
                    [alertaddjob show];
                }
            }
        }
        else
        {
            NSString *check = [newjobtitletextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if(![check isEqualToString:@""])
            {
                if([check length] > 32)
                {
                    [self.view makeToast:@"Job name is too, please shorten it!" duration:(0.3) position:@"center"];
                    [newjobtitletextfield becomeFirstResponder];
                    [alert show];
                }
                else
                {
                    [alert dismissWithClickedButtonIndex:1 animated:NO];
                    [self.view makeToast:@"Successfully created new job！" duration:(0.3) position:@"center"];
                    NSDate *today = [NSDate date];
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    dateFormat.locale = [NSLocale currentLocale];
                    [dateFormat setDateFormat:@"EEEE, dd MMMM YYYY"];
                    NSString *dateString = [dateFormat stringFromDate:today];
                    [dateFormat release];
                    
                    Myjob *myjob = [[Myjob alloc] init];
                    myjob.myjobtitle = [newjobtitletextfield.text stringByReplacingOccurrencesOfString:@"'" withString:@"\""];;
                    myjob.myjobdate = dateString;
                    [db insertMyjob:myjob];
                    [myjob release];
                    
                    [addjobpop dismissPopoverAnimated:YES];
                    
                    self.popcodelabel.text = [productcell.code.text substringFromIndex:7];
                    
                    joblist = [db retrieveMyjob];
                    [self.jobtable reloadData];
                    
                    addjobpop = [[UIPopoverController alloc] initWithContentViewController:self.addjobcontroller];
                    addjobpop.delegate = self;
                    [addjobpop presentPopoverFromRect:self.addjobbutton.frame inView:self.detailview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
                }
            }
            else
            {
                [self.view makeToast:@"Please give new job a name!" duration:(0.3) position:@"center"];
                [newjobtitletextfield becomeFirstResponder];
                [alert show];
            }
        }
    }
}
///////////

//enquiry//
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [popfirstnametextfield resignFirstResponder];
    [poplastnametextfield resignFirstResponder];
    [popemailtextfield resignFirstResponder];
    [popaddresstextfield resignFirstResponder];
    [popcompanytextfield resignFirstResponder];
    [popphonetextfield resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.frame.size.height + textField.frame.origin.y > self.view.frame.size.width - 621)
    {
        float covered = (textField.frame.size.height + textField.frame.origin.y) - (self.view.frame.size.width - 661);
        CGPoint bottomOffset = CGPointMake(0, covered);
        [self.popscrollview setContentOffset: bottomOffset animated: YES];
    }
    self.popscrollview.contentSize = CGSizeMake(self.popscrollview.contentSize.width, self.popscrollview.contentSize.height + 250);
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.popscrollview.contentSize = CGSizeMake(420, 410);
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]){
        [popcommentstextview resignFirstResponder];
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView.frame.size.height + textView.frame.origin.y > self.view.frame.size.width - 621)
    {
        float covered = textView.frame.size.height + textView.frame.origin.y - (self.view.frame.size.width - 621);
        CGPoint bottomOffset = CGPointMake(0, covered);
        [self.popscrollview setContentOffset: bottomOffset animated: YES];
        self.popscrollview.contentSize = CGSizeMake(self.popscrollview.contentSize.width, self.popscrollview.contentSize.height + 250);
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.popscrollview.contentSize = CGSizeMake(420, 410);
}

-(IBAction)subcribe:(id)sender
{
    issubcribe = !issubcribe;
    if(issubcribe)
    {
        self.popselectedbutton.enabled = NO;
        self.popnotselectedbutton.enabled = YES;
        [self.popselectedbutton setBackgroundImage:[UIImage imageNamed:@"IsSelected.png"] forState:UIControlStateNormal];
        [self.popnotselectedbutton setBackgroundImage:[UIImage imageNamed:@"NotSelected.png"] forState:UIControlStateNormal];
    }
    else
    {
        self.popselectedbutton.enabled = YES;
        self.popnotselectedbutton.enabled = NO;
        [self.popselectedbutton setBackgroundImage:[UIImage imageNamed:@"NotSelected.png"] forState:UIControlStateNormal];
        [self.popnotselectedbutton setBackgroundImage:[UIImage imageNamed:@"IsSelected.png"] forState:UIControlStateNormal];
    }
}

-(IBAction)submit:(id)sender
{
    NSString *check = [popfirstnametextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(![check isEqualToString:@""])
    {
        NSString *check = [poplastnametextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if(![check isEqualToString:@""])
        {
            NSString *check = [popemailtextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if(![check isEqualToString:@""])
            {
                [GlobalFunction AddLoadingScreen:self];
                [self performSelector:@selector(delay4) withObject:nil afterDelay:0.1];
            }
        }
    }
}

-(void)delay4
{
    NSString *decision;
    if(issubcribe)
        decision = @"Yes";
    else
        decision = @"No";
    
    WebServices *ws = [[WebServices alloc] init];
    
    NSString *reply = [ws sendEnquiry:popfirstnametextfield.text :poplastnametextfield.text :popemailtextfield.text :popphonetextfield.text :popcompanytextfield.text :popaddresstextfield.text :popproductcodetextfield.text :decision :popcommentstextview.text];
    
    [ws release];
    
    popfirstnametextfield.text = @"";
    poplastnametextfield.text = @"";
    popemailtextfield.text = @"";
    popphonetextfield.text = @"";
    popcommentstextview.text = @"";
    popcompanytextfield.text = @"";
    popaddresstextfield.text = @"";
    popproductcodetextfield.text = @"";
    issubcribe = YES;
    self.popselectedbutton.enabled = NO;
    self.popnotselectedbutton.enabled = YES;
    [self.popselectedbutton setBackgroundImage:[UIImage imageNamed:@"IsSelected.png"] forState:UIControlStateNormal];
    [self.popnotselectedbutton setBackgroundImage:[UIImage imageNamed:@"NotSelected.png"] forState:UIControlStateNormal];
    
    [GlobalFunction RemoveLoadingScreen:self];
    
    if([reply isEqualToString:@"Success"])
    {
        alertenquiry = [[UIAlertView alloc] initWithTitle:@"THANK YOU FOR YOUR INQUIRY" message:@"A CRH team member will contact you as soon as possible." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
    }
    else
    {
        alertenquiry = [[UIAlertView alloc] initWithTitle:@"ERROR OCCURED" message:@"Enquiry sent failed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    }
    
    [alertenquiry show];
    [alertenquiry release];
}

-(IBAction)cancel:(id)sender
{
    [self.enquiryview removeFromSuperview];
}
////////////////////////

///////email////////////
-(IBAction)sendwithprice:(id)sender
{
    isneedprice = YES;
    [emailpop dismissPopoverAnimated:YES];
    sendpop = [[UIPopoverController alloc] initWithContentViewController:self.sendcontroller];
    sendpop.delegate = self;
    [sendpop presentPopoverFromRect:self.emailbutton.frame inView:self.detailview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

-(IBAction)sendwithoutprice:(id)sender
{
    isneedprice = NO;
    [emailpop dismissPopoverAnimated:YES];
    sendpop = [[UIPopoverController alloc] initWithContentViewController:self.sendcontroller];
    sendpop.delegate = self;
    [sendpop presentPopoverFromRect:self.emailbutton.frame inView:self.detailview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

-(IBAction)sendplain:(id)sender
{
    if([GlobalFunction NetworkStatus])
    {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            [mailer setSubject:@"CRH Product Bookmarks"];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            NSString *emailBody;
            
            if(isneedprice)
            {
                emailBody = [NSString stringWithFormat:@"\n\n\n%@\n\n%@\n\n%@\n\n%@\n\n%@", titleOfRowSelected, self.codelabel.text, self.tradelabel.text, self.retaillabel.text, descriptionlabel.text];
            }
            else
            {
                emailBody = [NSString stringWithFormat:@"\n\n\n%@\n\n%@\n\n%@", titleOfRowSelected, self.codelabel.text, descriptionlabel.text];
            }
            
            [mailer setMessageBody:emailBody isHTML:NO];
            
            if(mailer != nil)
            {
                // only for iPad
                mailer.modalPresentationStyle = UIModalPresentationPageSheet;
                
                [self presentModalViewController:mailer animated:YES ];
            }
            
            [mailer release];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
}

-(IBAction)sendhtml:(id)sender
{
    if([GlobalFunction NetworkStatus])
    {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            [mailer setSubject:@"CRH Product Bookmarks"];
            
            NSString *emailBody;
            
            if(isneedprice)
                emailBody = [NSString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%@<br /><br />%@<br /><br />%@<br /><br />%@<br /><br />%@", imagepathM, titleOfRowSelected, self.codelabel.text, self.tradelabel.text, self.retaillabel.text, descriptionlabel.text];
            else
                emailBody = [NSString stringWithFormat:@"<br /><br /><br /><img src=\"%@\"><br /><br />%@<br /><br />%@<br /><br />%@", imagepathM, titleOfRowSelected, self.codelabel.text, descriptionlabel.text];
            
            [mailer setMessageBody:emailBody isHTML:YES];
            
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];
            
            [mailer release];
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
}

-(IBAction)sendpdf:(id)sender
{
    if([GlobalFunction NetworkStatus])
    {
        [GlobalFunction AddLoadingScreen:self];
        
        [self.view makeToast:@"Generating PDF..." duration:(0.3) position:@"center"];
        
        [self performSelector:@selector(delay6) withObject:nil afterDelay:0.3];
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
}

-(void)delay6
{
    //NSLog(@"called me when u r generating pdf !!! btw... me is childScene");
    
    if([GlobalFunction NetworkStatus])
    {
        if ([MFMailComposeViewController canSendMail])
        {
            NSString * timeStampValue = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
            
            NSString *try = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [middleImageArr objectAtIndex:productrowselected]];
            
            NSString *filterStr = [try stringByReplacingOccurrencesOfString:@"http://crh-app.novafusion.net:8052" withString:@".."];
            
            NSString *filterDescStr = [descriptionlabel.text stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
            
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"ºF" withString:@"fahrenheitSign"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"º" withString:@"degreeSign"];
            filterDescStr = [filterDescStr stringByReplacingOccurrencesOfString:@"®" withString:@"(REGISTERED)"];
            
            NSString *filterTitleStr = [titleOfRowSelected stringByReplacingOccurrencesOfString:@"ºC" withString:@"celciusSign"];
            filterTitleStr = [filterTitleStr stringByReplacingOccurrencesOfString:@"\"" withString:@"12345_"];
            
            WebServices *ws = [[WebServices alloc] init];
            
            NSString *reply = [[NSString alloc] init];
            
            NSMutableString *emailBody = [[NSMutableString alloc] init];
            
            if(isneedprice)
            {
                reply = [ws generatePDF_FileWithPrice:filterTitleStr :self.codelabel.text :self.tradelabel.text :self.retaillabel.text :filterDescStr :filterStr :timeStampValue];
            }
            else
            {
                reply = [ws generatePDF_FileWithoutPrice:filterTitleStr :self.codelabel.text:filterDescStr:filterStr :timeStampValue];
            }
            
            [ws release];
            
            if([reply isEqualToString:@"Success"])
            {
                NSString *pdf_link = [NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/tiseno/fpdf17/product_pdf/%@.pdf", timeStampValue];
                
                MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
                
                mailer.mailComposeDelegate = self;
                
                [mailer setSubject:@"CRH Product Bookmarks"];
                
                [emailBody appendString:@"<br /><br />"];
                [emailBody appendString:[NSString stringWithFormat:@"<br /><br /><br /><br />Click the link to download the pdf file :<br /> <a href=\"%@\">%@.pdf</a>", pdf_link, timeStampValue]];
                
                [mailer setMessageBody:emailBody isHTML:YES];
                
                mailer.modalPresentationStyle = UIModalPresentationPageSheet;
                
                [self presentModalViewController:mailer animated:YES];
                
                [mailer release];
                [emailBody release];
            }
            else
            {
                [self.view makeToast:@"Failed to generate PDF..." duration:(0.3) position:@"center"];
            }
        }
        else
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Your device doesn't support the composer sheet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
    }
    else
    {
        [self.view makeToast:@"No Internet Connection！" duration:(0.3) position:@"center"];
    }
    
    [GlobalFunction RemoveLoadingScreen:self];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissModalViewControllerAnimated:YES];
}
//////////////////

//search//
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchbar resignFirstResponder];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0)
    {
        [self resetSearch];
        [self.categorytable reloadData];
        return;
    }
    [self filterContentForSearchText:searchText];
}

-(void)resetSearch
{
    self.categoryArr = beforesearch;
    [self.categorytable reloadData];
}

- (void)filterContentForSearchText:(NSString*)searchText
{
	categorylist = [self.categoryArr mutableCopy];
	[categorylist removeAllObjects];
    
	for (int i = 0; i < self.categoryArr.count;i++)
	{
        NSDictionary* novainfo = [self.categoryArr objectAtIndex:i];
        
        NSDictionary* novadetails = [novainfo objectForKey:@"category"];
        
        NSString *categorytitle = [novadetails objectForKey:@"name"];
        
        if ([categorytitle rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            continue;
        }
        else
        {
            [categorylist addObject:[self.categoryArr objectAtIndex:i]];
        }
	}
    
    self.categoryArr = categorylist;
    [self.categorytable reloadData];
}

@end
