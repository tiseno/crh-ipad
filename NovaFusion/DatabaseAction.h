//
//  DatabaseAction.h
//  OrientalDailyiOS
//
//  Created by Tiseno on 8/9/12.
//  Copyright (c) 2012 Tiseno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQLite.h"
#import "Bookmark.h"
#import "Myjob.h"
#import "Product.h"

@interface DatabaseAction : SQLite{
    BOOL runBatch;
}

@property (nonatomic)BOOL runBatch;

-(DataBaseInsertionResult)insertBookmark:(Bookmark*)bookmark;
-(DataBaseInsertionResult)insertMyjob:(Myjob*)myjob;
-(DataBaseInsertionResult)insertProduct:(Product*)product;

-(NSArray*)retrieveBookmark;
-(NSArray*)retrieveMyjob;
-(NSArray*)retrieveProduct:(int)myjobid;
-(NSArray*)retrieveAllproduct;

-(NSString*)retrieveSelectedmyjobtitle:(int)myjobid;

-(int)retrieveBookmarkselectedrow:(int)row;
-(int)retrieveMyjobselectedrow:(int)row;
-(int)retrieveProductselectedrow:(int)row:(int)myjobid;

-(int)countProductinjob:(int)myjobid:(NSString *)code;
-(int)retrieveProductQuantity:(int)myjobid:(NSString *)code;

-(DataBaseUpdateResult)updateProductQuantity:(int)myjobid:(NSString *)code:(int)quantity;
-(DataBaseUpdateResult)updateMyjobtitle:(int)myjobid:(NSString *)title;

-(DataBaseDeletionResult)deleteAllbookmark;
-(DataBaseDeletionResult)deleteBookmark:(int)bookmarkid;
-(DataBaseDeletionResult)deleteAllmyjob;
-(DataBaseDeletionResult)deleteMyjob:(int)myjobid;
-(DataBaseDeletionResult)deleteAllproduct;
-(DataBaseDeletionResult)deleteProduct:(int)productid:(int)myjobid;
-(DataBaseDeletionResult)deleteProductWithCode:(NSString*)code:(int)myjobid;
-(DataBaseDeletionResult)deleteProductWithJobid:(int)myjobid;

@end
