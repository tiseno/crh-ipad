//
//  ViewController.h
//  NovaFusion
//
//  Created by tiseno on 9/28/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainScene.h"
#import "WebServices.h"
#import "nova_application.h"

@interface ViewController : UIViewController
{
    BOOL isconnected;
    NSTimer *connectiontimer;
    UIAlertView *alert;
}

@property (retain, nonatomic) NSArray *mainArr;

@end
