//
//  CategoryCell.h
//  NovaFusion
//
//  Created by tiseno on 10/1/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CategoryCell : UITableViewCell{
    
}

@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UIView *separator;
@property (nonatomic, retain) UIButton *thumbnailbutton;
@property (nonatomic, retain) CAGradientLayer *gradient;

@end
