//
//  GlobalFunction.h
//  NovaFusion
//
//  Created by tiseno on 9/28/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface GlobalFunction : NSObject{
    
}

+(BOOL)NetworkStatus;

+(void)AddLoadingScreen:(UIViewController *)controller;
+(void)RemoveLoadingScreen:(UIViewController *)controller;

@end
