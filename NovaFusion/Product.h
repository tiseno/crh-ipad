//
//  Product.h
//  NovaFusion
//
//  Created by tiseno on 10/9/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject
{
    
}

@property (retain, nonatomic) NSString *producttitle, *productcode, *producttrade, *productretail, *productdescription, *productimagepathS, *productimagepathM;

@property (nonatomic) int productquantity, myjobid, sectionid, categoryid;;

@end
