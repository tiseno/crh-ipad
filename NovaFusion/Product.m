//
//  Product.m
//  NovaFusion
//
//  Created by tiseno on 10/9/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize producttitle, productcode, productimagepathS, productimagepathM, producttrade, productretail, productdescription, productquantity, myjobid, sectionid, categoryid;

@end
