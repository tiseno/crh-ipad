//
//  JobCell.m
//  NovaFusion
//
//  Created by tiseno on 10/4/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import "JobCell.h"

@implementation JobCell

@synthesize jobtitle, jobdescription;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *jobTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, 120, 18)];
        jobTitle.textAlignment = UITextAlignmentLeft;
        jobTitle.textColor = [UIColor blackColor];
        jobTitle.backgroundColor = [UIColor clearColor];
        jobTitle.font = [UIFont boldSystemFontOfSize:14];
        jobTitle.numberOfLines = 1;
        self.jobtitle = jobTitle;
        [jobTitle release];
        [self addSubview:jobtitle];
        
        UILabel *jobDescription = [[UILabel alloc] initWithFrame:CGRectMake(20, 25, 120, 13)];
        jobDescription.textAlignment = UITextAlignmentLeft;
        jobDescription.textColor = [UIColor grayColor];
        jobDescription.backgroundColor = [UIColor clearColor];
        jobDescription.font = [UIFont systemFontOfSize:12];
        jobDescription.lineBreakMode = UILineBreakModeWordWrap;
        jobDescription.numberOfLines = 1;
        self.jobdescription = jobDescription;
        [jobDescription release];
        [self addSubview:jobdescription];
        
        UIImageView *Separator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 39.5, 300, 0.5)];
        Separator.backgroundColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0];
        [self addSubview:Separator];
        [Separator release];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [jobtitle release];
    [jobdescription release];
    [super dealloc];
}

@end
