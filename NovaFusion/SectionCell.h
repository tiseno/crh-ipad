//
//  SectionCell.h
//  NovaFusion
//
//  Created by tiseno on 10/1/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionCell : UITableViewCell{
    
}

@property (nonatomic, retain) UILabel *title, *description;
@property (nonatomic, retain) UIImageView *sectionimageview;

@end
