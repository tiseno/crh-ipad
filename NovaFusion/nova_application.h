//
//  nova_application.h
//  NovaFusion
//
//  Created by Jermin Bazazian on 10/2/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface nova_application : NSObject{}

@property (nonatomic, retain) NSString *name, *alias, *application_group, *description, *params;

@end
