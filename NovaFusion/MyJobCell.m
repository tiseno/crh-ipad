//
//  MyJobCell.m
//  NovaFusion
//
//  Created by tiseno on 10/4/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import "MyJobCell.h"

@implementation MyJobCell

@synthesize myjobtitle, myjobdate, arrowimage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *myjobTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 200, 25)];
        myjobTitle.textAlignment = UITextAlignmentLeft;
        myjobTitle.textColor = [UIColor blackColor];
        myjobTitle.backgroundColor = [UIColor clearColor];
        myjobTitle.font = [UIFont boldSystemFontOfSize:16];
        myjobTitle.lineBreakMode = UILineBreakModeWordWrap;
        myjobTitle.numberOfLines = 0;
        self.myjobtitle = myjobTitle;
        [myjobTitle release];
        [self addSubview:myjobtitle];
        
        UILabel *myjobDate = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, 200, 15)];
        myjobDate.textAlignment = UITextAlignmentLeft;
        myjobDate.textColor = [UIColor grayColor];
        myjobDate.backgroundColor = [UIColor clearColor];
        myjobDate.font = [UIFont systemFontOfSize:13];
        myjobDate.lineBreakMode = UILineBreakModeWordWrap;
        myjobDate.numberOfLines = 1;
        self.myjobdate = myjobDate;
        [myjobDate release];
        [self addSubview:myjobdate];
        
        UIImageView *ArrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_grey.png"]];
        self.arrowimage = ArrowImage;
        [ArrowImage release];
        [self addSubview:arrowimage];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [myjobtitle release];
    [myjobdate release];
    [arrowimage release];
    [super dealloc];
}

@end
