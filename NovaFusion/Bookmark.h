//
//  Bookmark.h
//  NovaFusion
//
//  Created by tiseno on 10/9/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bookmark : NSObject
{
    
}

@property (retain, nonatomic) NSString *bookmarktitle, *bookmarkcode, *bookmarktrade, *bookmarkretail, *bookmarkdescription, *bookmarkimagepathS, *bookmarkimagepathM;

@property (nonatomic) int sectionid, categoryid;

@end
