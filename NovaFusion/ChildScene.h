//
//  ChildScene.h
//  NovaFusion
//
//  Created by tiseno on 9/28/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#import "Reachability.h"
#import "Toast+UIView.h"
#import "GlobalFunction.h"
#import "WebServices.h"
#import "DatabaseAction.h"
#import "Bookmark.h"
#import "Myjob.h"
#import "Product.h"
#import "AsyncImageView.h"
#import "CategoryCell.h"
#import "ProductCell.h"
#import "JobCell.h"

@interface ChildScene : UIViewController<UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate, UITextFieldDelegate, UITextViewDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate, UISearchBarDelegate>{
    CategoryCell *categorycell;
    ProductCell *productcell;
    JobCell *jobcell;
    UIPopoverController *addjobpop, *emailpop, *sendpop;
    float red, green, blue;
    int rowselected, i1, inviewpage, result, productrowselected, isconnected, buttontag;
    UIAlertView *alert, *alertenquiry, *alertaddjob;
    BOOL isneedprice, istechnical, isadd, isexist, issubcribe;
    UILabel *descriptionlabel;
    UITextField *newjobtitletextfield, *quantitytextfield;
    NSString *imagepathM, *titleOfRowSelected, *productCodeOfRowSelected;
    NSArray *joblist, *productlist, *bookmarklist, *itemstemp;
    DatabaseAction *db;
    NSTimer *connectiontimer, *checkimagetimer;
    UIView *transparentview;
    NSMutableArray *categorylist, *beforesearch;
}

//main//
@property (retain, nonatomic) IBOutlet UIView *categoryview;
@property (retain, nonatomic) IBOutlet UIImageView *categoryheaderbg;
@property (retain, nonatomic) IBOutlet UITableView *categorytable;
@property (retain, nonatomic) IBOutlet UIView *productview;
@property (retain, nonatomic) IBOutlet UITableView *producttable;
@property (retain, nonatomic) IBOutlet UITableView *thumbnailtable;
@property (retain, nonatomic) IBOutlet UIImageView *borderimage;
@property (retain, nonatomic) IBOutlet UIImageView *productimage;
@property (retain, nonatomic) IBOutlet UIImageView *buttonbgimage;
@property (retain, nonatomic) IBOutlet UIScrollView *detailscrollview;
@property (retain, nonatomic) IBOutlet UILabel *titlelabel;
@property (retain, nonatomic) IBOutlet UILabel *codelabel;
@property (retain, nonatomic) IBOutlet UILabel *tradelabel;
@property (retain, nonatomic) IBOutlet UILabel *retaillabel;
@property (retain, nonatomic) IBOutlet UIView *detailview;
@property (retain, nonatomic) IBOutlet UILabel *headerlabel;
@property (retain, nonatomic) IBOutlet UILabel *categorylabel;
@property (retain, nonatomic) IBOutlet UILabel *seriallabel;
@property (retain, nonatomic) IBOutlet UIImageView *titlebgimage;

@property (retain, nonatomic) IBOutlet UIView *searchview;
@property (retain, nonatomic) IBOutlet UISearchBar *searchbar;

@property (retain, nonatomic) IBOutlet UIButton *addjobbutton;
@property (retain, nonatomic) IBOutlet UIButton *enquirybutton;
@property (retain, nonatomic) IBOutlet UIButton *emailbutton;

-(IBAction)back:(id)sender;
-(IBAction)search:(id)sender;
-(IBAction)zoomin:(id)sender;
-(IBAction)drawing:(id)sender;
-(IBAction)bookmarks:(id)sender;
-(IBAction)add:(id)sender;
-(IBAction)enquiry:(id)sender;
-(IBAction)email:(id)sender;

-(IBAction)cancelsearch:(id)sender;

@property (retain, nonatomic) NSMutableArray *categoryArr;
@property (retain, nonatomic) NSString *categorytext;
@property (retain, nonatomic) NSArray *itemArr;
@property (nonatomic) int row;
@property (retain, nonatomic) NSMutableArray *totalItem;

@property (retain, nonatomic) NSMutableArray *itemPriceArr;
@property (retain, nonatomic) NSMutableArray *middleImageArr;
@property (retain, nonatomic) NSMutableArray *tradeArr;
@property (retain, nonatomic) NSArray *technicalImageArr;
@property (retain, nonatomic) NSArray *galleryImageArr;

//popupimage//
@property (retain, nonatomic) IBOutlet UIView *popupview;
@property (retain, nonatomic) IBOutlet UIImageView *popupimage;
@property (retain, nonatomic) IBOutlet UILabel *popuplabel;
@property (retain, nonatomic) IBOutlet UIButton *popuppreviousbutton;
@property (retain, nonatomic) IBOutlet UIButton *popupnextbutton;
@property (retain, nonatomic) IBOutlet UIButton *popupclosebutton;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *loadingview;

-(IBAction)closepopup:(id)sender;
-(IBAction)next:(id)sender;
-(IBAction)previous:(id)sender;
/////////////
////////

//addjob//
@property (retain, nonatomic) IBOutlet UILabel *popcodelabel;
@property (retain, nonatomic) IBOutlet UILabel *popdescriptionlabel;
@property (retain, nonatomic) IBOutlet UITableView *jobtable;

-(IBAction)newjob:(id)sender;
//////////

//enquiry//
@property (retain, nonatomic) IBOutlet UIView *enquiryview;
@property (retain, nonatomic) IBOutlet UITextField *popfirstnametextfield;
@property (retain, nonatomic) IBOutlet UITextField *poplastnametextfield;
@property (retain, nonatomic) IBOutlet UITextField *popemailtextfield;
@property (retain, nonatomic) IBOutlet UITextField *popphonetextfield;
@property (retain, nonatomic) IBOutlet UITextField *popaddresstextfield;
@property (retain, nonatomic) IBOutlet UITextField *popcompanytextfield;
@property (retain, nonatomic) IBOutlet UITextField *popproductcodetextfield;
@property (retain, nonatomic) IBOutlet UITextView *popcommentstextview;
@property (retain, nonatomic) IBOutlet UIButton *popselectedbutton;
@property (retain, nonatomic) IBOutlet UIButton *popnotselectedbutton;
@property (retain, nonatomic) IBOutlet UIScrollView *popscrollview;

-(IBAction)subcribe:(id)sender;
-(IBAction)submit:(id)sender;
-(IBAction)cancel:(id)sender;
///////////

//email//
-(IBAction)sendwithprice:(id)sender;
-(IBAction)sendwithoutprice:(id)sender;
-(IBAction)sendplain:(id)sender;
-(IBAction)sendhtml:(id)sender;
-(IBAction)sendpdf:(id)sender;
/////////

//popover//
@property (retain, nonatomic) IBOutlet UIViewController *addjobcontroller;
@property (retain, nonatomic) IBOutlet UIViewController *emailcontroller;
@property (retain, nonatomic) IBOutlet UIViewController *sendcontroller;
//////////

@property (retain, nonatomic) NSString *appID, *itemID, *viewOption;

@end
