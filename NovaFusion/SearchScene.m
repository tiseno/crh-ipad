//
//  SearchScene.m
//  NovaFusion
//
//  Created by tiseno on 10/17/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#define Height 110

#import "SearchScene.h"

@implementation SearchScene

@synthesize resulttable, searchbar, keywordlabel, keyword;

@synthesize itemCodeArr, itemImageArr, itemNameArr, itemPriceArr, itemAppIdArr, itemIdArr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.resulttable.hidden = YES;
    iswhite = YES;
    aftertotal = 0;
    if(itemNameArr.count <=0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Result!" message:@"Your search no result!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
    [self performSelector:@selector(delay) withObject:nil afterDelay:0.1];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    isconnected = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [connectiontimer invalidate];
    [self.itemNameArr removeAllObjects];
}

-(void)dealloc
{
    [resulttable release];
    [searchbar release];
    [keywordlabel release];
    
    [super dealloc];
}

//checking connection//
-(void)checkingconnection
{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemoveLoadingScreen:self];
        isconnected = YES;
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction AddLoadingScreen:self];
        alert = [[UIAlertView alloc] initWithTitle:@"Connection Lost!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        isconnected = NO;
    }
}
///////////////////////

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 660;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *hlCellID = @"hlCellID";
    
    UITableViewCell *hlcell = [tableView dequeueReusableCellWithIdentifier:hlCellID];
    if(hlcell == nil) {
        hlcell =  [[[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault reuseIdentifier:hlCellID] autorelease];
        hlcell.accessoryType = UITableViewCellAccessoryNone;
        hlcell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    n = max;
    int i1 = 0;
    
    for(int clear = 0; clear < 18; clear++)
    {
        UIView *existingToast = [hlcell.contentView viewWithTag:8888];
        if (existingToast != nil) {
            [existingToast removeFromSuperview];
        }
    }
    
    while(i - page * 18 <n)
    {
        int y = i1 * Height;
        int j=0;
        for(j=0; j<3;j++){
            if (i - page * 18 >=n)
                break;
            
            UIView *resultview = [[[UIView alloc]initWithFrame:CGRectMake(0 + j * ([[UIScreen mainScreen] bounds].size.height / 3), y, [[UIScreen mainScreen] bounds].size.height / 3, Height)] autorelease];
            if(iswhite)
            {
                resultview.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:243.0/255.0 alpha:1.0];
            }
            else
                resultview.backgroundColor = [UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0];
            
            resultview.tag = 8888;
            resultview.layer.borderColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.3].CGColor;
            resultview.layer.borderWidth = 1.0f;
            
            UIImageView *ProductImage = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 3, 83, 104)] autorelease];
            ProductImage.imageURL = [NSURL URLWithString:[itemImageArr objectAtIndex:i]];
            [resultview addSubview:ProductImage];
            
            UILabel *Title = [[[UILabel alloc] initWithFrame:CGRectMake(90, 20, 230, 30)] autorelease];
            Title.text = [itemNameArr objectAtIndex:i];
            Title.textAlignment = UITextAlignmentLeft;
            Title.textColor = [UIColor blackColor];
            Title.backgroundColor = [UIColor clearColor];
            Title.font = [UIFont systemFontOfSize:13];
            Title.lineBreakMode = UILineBreakModeWordWrap;
            Title.numberOfLines = 2;
            [resultview addSubview:Title];
            
            UILabel *Code = [[[UILabel alloc] initWithFrame:CGRectMake(90, 50, 180, 20)] autorelease];
            Code.text = [NSString stringWithFormat:@"Code: %@", [itemCodeArr objectAtIndex:i]];
            Code.textAlignment = UITextAlignmentLeft;
            Code.textColor = [UIColor grayColor];
            Code.backgroundColor = [UIColor clearColor];
            Code.font = [UIFont systemFontOfSize:12];
            Code.lineBreakMode = UILineBreakModeWordWrap;
            Code.numberOfLines = 1;
            [resultview addSubview:Code];
            
            UILabel *Retail = [[[UILabel alloc] initWithFrame:CGRectMake(90, 70, 180, 20)] autorelease];
            Retail.text = [NSString stringWithFormat:@"Retail: $%.2f", [[itemPriceArr objectAtIndex:i] floatValue]];
            Retail.textAlignment = UITextAlignmentLeft;
            Retail.textColor = [UIColor grayColor];
            Retail.backgroundColor = [UIColor clearColor];
            Retail.font = [UIFont systemFontOfSize:12];
            Retail.lineBreakMode = UILineBreakModeWordWrap;
            Retail.numberOfLines = 1;
            [resultview addSubview:Retail];
            
            UIButton *view=[[[UIButton alloc] initWithFrame:CGRectMake(275, 70, 50, 25)] autorelease];
            [view setBackgroundImage:[UIImage imageNamed:@"btn_view_blue.png"]    forState:UIControlStateNormal];
            view.tag = i;
            [view addTarget:self action:@selector(productdetailview:) forControlEvents:UIControlEventTouchUpInside];
            [resultview addSubview:view];
            
            [hlcell.contentView addSubview:resultview];
            i++;
        }
        i1 = i1+1;
        iswhite = !iswhite;
    }
    
    return hlcell;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar
{
    [self.searchbar resignFirstResponder];
    self.resulttable.hidden = YES;
    iswhite = YES;
    [GlobalFunction AddLoadingScreen:self];
    [self performSelector:@selector(delay3) withObject:nil afterDelay:0.1];
}

-(void)delay
{
    if(itemNameArr.count > 0)
    {
        page = 0;
        temp = itemNameArr.count;
        totalpage = temp / 18;
        max = 18;
        int reminder = temp - (temp / 18) * 18;
        if(reminder != 0)
            totalpage++;
        if(totalpage == 1)
        {
            max = temp % 18;
        }
        else if(totalpage > 18)
        {
            totalpage = 18;
        }
        
        for(int count = 0; count < aftertotal; count++)
        {
            UIView *existingToast = [self.view viewWithTag:count + 1000];
            if (existingToast != nil) {
                [existingToast removeFromSuperview];
            }
        }
        
        aftertotal = totalpage;
        
        for(int count = 0; count < totalpage; count++)
        {
            
            UIButton *pagebutton;
            if(count > 8)
            {
                UIView *existingToast = [self.view viewWithTag:count+999];
                pagebutton = [[[UIButton alloc]initWithFrame:CGRectMake(existingToast.frame.origin.x + existingToast.frame.size.width, 51, 55, 30)] autorelease];
            }
            else
                pagebutton = [[[UIButton alloc]initWithFrame:CGRectMake(80 + count * 45, 51, 45, 30)] autorelease];
            
            pagebutton.tag = count + 1000;
            
            if(count == 0)
            {
                [pagebutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            else
            {
                [pagebutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }
            
            [pagebutton setTitle:[NSString stringWithFormat:@"Page %d", count+1] forState:UIControlStateNormal];
            pagebutton.font = [UIFont systemFontOfSize:13];
            [pagebutton addTarget:self action:@selector(changepage:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:pagebutton];
        }
        
        [self.resulttable reloadData];
        self.resulttable.hidden = NO;
    }
    self.keywordlabel.text = keyword;
}

-(void)delay3
{
    NSError *error;
    
    WebServices *ws = [[WebServices alloc] init];
    
    NSArray *result = [ws searchItemByKeywords:self.searchbar.text];
    
    if(result.count > 0)
    {
        itemCodeArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemImageArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemNameArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemPriceArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        
        itemAppIdArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        itemIdArray = [[[NSMutableArray alloc] initWithCapacity:result.count+1] retain];
        
        for(int j = 0; j < result.count; j++)
        {
            NSDictionary* novainfo = [result objectAtIndex:j];
            
            NSDictionary* novadetails = [novainfo objectForKey:@"item"];
            
            [itemNameArray addObject:[novadetails objectForKey:@"name"]];
            
            NSDictionary *elementDetails = [NSJSONSerialization JSONObjectWithData: [[novadetails objectForKey:@"elements"] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &error];
            
            //Print the price of the product
            NSDictionary *priceDetails = [elementDetails objectForKey:@"db4b1af4-29b4-4432-8acb-5ade2510745a"];
            NSDictionary *price = [priceDetails objectForKey:@"0"];
            
            [itemPriceArray addObject:[price objectForKey:@"value"]];
            
            NSDictionary *itemDic = [elementDetails objectForKey:@"3e1dc070-343b-40cb-a8dc-b419d1a01b60"];
            NSDictionary *itemCode = [itemDic objectForKey:@"0"];
            
            [itemCodeArray addObject:[itemCode objectForKey:@"value"]];
            
            NSDictionary *thumbpath = [elementDetails objectForKey:@"c0794696-a5d2-4df9-b20f-f23b0f94a41b"];
            
            [itemImageArray addObject:[NSString stringWithFormat:@"http://crh-app.novafusion.net:8052/%@", [thumbpath objectForKey:@"file"]]];
            
            [itemAppIdArray addObject:[novadetails objectForKey:@"application_id"]];
            
            [itemIdArray addObject:[novadetails objectForKey:@"id"]];
        }
    }
    
    [ws release];
    [GlobalFunction RemoveLoadingScreen:self];
    
    SearchScene *GtestClasssViewController=[[[SearchScene alloc] initWithNibName:@"SearchScene"  bundle:nil] autorelease];
    
    GtestClasssViewController.keyword = self.searchbar.text;
    GtestClasssViewController.itemCodeArr = itemCodeArray;
    GtestClasssViewController.itemImageArr = itemImageArray;
    GtestClasssViewController.itemNameArr = itemNameArray;
    GtestClasssViewController.itemPriceArr = itemPriceArray;
    GtestClasssViewController.itemAppIdArr = itemAppIdArray;
    GtestClasssViewController.itemIdArr = itemIdArray;
    
    [self presentModalViewController:GtestClasssViewController animated:NO];
}

-(IBAction)changepage:(id)sender
{
    int tag = [sender tag] - 1000;
    if(tag != page)
    {
        page = tag;
        i = page * 18;
        if(page == totalpage-1)
        {
            max = temp % 18;
        }
        else if (totalpage == 1)
        {
            max = temp % 18;
        }
        else
            max = 18;
        
        for(UIButton *button in self.view.subviews)
        {
            if(button.tag > 999)
            {
                if(button.tag == [sender tag])
                {
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }
                else
                {
                    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
            }
        }
        
        [self.resulttable reloadData];
    }
}

-(IBAction)productdetailview:(id)sender
{
    itemselected = [sender tag];
    
    [self performSelector:@selector(delay2) withObject:nil afterDelay:0.1];
}

-(void)delay2
{
    ChildScene *GtestClasssViewController=[[[ChildScene alloc] initWithNibName:@"ChildScene"  bundle:nil] autorelease];
    GtestClasssViewController.appID = [itemAppIdArr objectAtIndex:itemselected];
    GtestClasssViewController.itemID = [itemIdArr objectAtIndex:itemselected];
    GtestClasssViewController.viewOption = @"1";
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)back:(id)sender
{
    [super dismissModalViewControllerAnimated:YES];
}

@end
