//
//  SearchScene.h
//  NovaFusion
//
//  Created by tiseno on 10/17/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "WebServices.h"
#import "GlobalFunction.h"
#import "ChildScene.h"

@interface SearchScene : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
{
    int page, totalpage, aftertotal, temp, n, i, max, counttimes, itemselected;
    BOOL isconnected, iswhite;
    NSTimer *connectiontimer;
    UIAlertView *alert;
    
    NSMutableArray *itemNameArray, *itemCodeArray, *itemPriceArray, *itemImageArray, *itemAppIdArray, *itemIdArray;
}

@property (retain, nonatomic) IBOutlet UITableView *resulttable;
@property (retain, nonatomic) IBOutlet UISearchBar *searchbar;
@property (retain, nonatomic) IBOutlet UILabel *keywordlabel;

-(IBAction)back:(id)sender;

@property (retain, nonatomic) NSString *keyword;

@property (retain, nonatomic) NSMutableArray *itemNameArr, *itemCodeArr, *itemPriceArr, *itemImageArr, *itemAppIdArr, *itemIdArr;

@end
