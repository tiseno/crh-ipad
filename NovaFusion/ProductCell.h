//
//  ProductCell.h
//  NovaFusion
//
//  Created by tiseno on 10/1/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/CALayer.h>

@interface ProductCell : UITableViewCell{
    
}

@property (nonatomic, retain) UILabel *title, *code, *description, *serial;
@property (nonatomic, retain) UIImageView *productimage, *serialimage;
@property (nonatomic, retain) UIView *backgroundview;

@end
