//
//  JobCell.h
//  NovaFusion
//
//  Created by tiseno on 10/4/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobCell : UITableViewCell{
    
}

@property (nonatomic, retain) UILabel *jobtitle, *jobdescription;

@end
