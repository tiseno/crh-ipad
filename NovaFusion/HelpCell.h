//
//  HelpCell.h
//  NovaFusion
//
//  Created by tiseno on 10/10/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCell : UITableViewCell
{
    
}

@property (nonatomic, retain) UILabel *helpdescription;
@property (nonatomic, retain) UIImageView *imageview, *dotimage;

@end
