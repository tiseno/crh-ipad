//
//  SectionCell.m
//  NovaFusion
//
//  Created by tiseno on 10/1/12.
//  Copyright (c) 2012 tiseno. All rights reserved.
//

#import "SectionCell.h"

@implementation SectionCell

@synthesize title, description, sectionimageview;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIImageView *SectionImage = [[UIImageView alloc] initWithFrame:CGRectMake(52, 0, 105, 105)];
        SectionImage.backgroundColor = [UIColor clearColor];
        self.sectionimageview = SectionImage;
        [SectionImage release];
        [self addSubview:sectionimageview];
        
        UILabel *Title = [[UILabel alloc] initWithFrame:CGRectMake(250, 20, 650, 20)];
        Title.textAlignment = UITextAlignmentLeft;
        Title.textColor = [UIColor blackColor];
        Title.backgroundColor = [UIColor clearColor];
        Title.font = [UIFont boldSystemFontOfSize:18];
        Title.lineBreakMode = UILineBreakModeWordWrap;
        Title.numberOfLines = 0;
        self.title = Title;
        [Title release];
        [self addSubview:title];
        
        UILabel *Description = [[UILabel alloc] initWithFrame:CGRectMake(250, 40, 650, 46)];
        Description.textAlignment = UITextAlignmentLeft;
        Description.textColor = [UIColor grayColor];
        Description.backgroundColor = [UIColor clearColor];
        Description.font = [UIFont systemFontOfSize:13];
        Description.lineBreakMode = UILineBreakModeWordWrap;
        Description.numberOfLines = 0;
        self.description = Description;
        [Description release];
        [self addSubview:description];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [title release];
    [description release];
    [sectionimageview release];
    
    [super dealloc];
}

@end
