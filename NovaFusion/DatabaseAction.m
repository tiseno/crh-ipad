//
//  DatabaseAction.m
//  OrientalDailyiOS
//
//  Created by Tiseno on 8/9/12.
//  Copyright (c) 2012 Tiseno. All rights reserved.
//

#import "DatabaseAction.h"

@implementation DatabaseAction

@synthesize runBatch;

-(DataBaseInsertionResult)insertBookmark:(Bookmark*)bookmark
{
    BOOL connectionIsOpenned=YES;
    
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    
    if(connectionIsOpenned)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into Bookmark(Title, Code, Trade, Retail, Description, ImagepathS, ImagepathM, SectionID, CategoryID) values('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%d', '%d');", bookmark.bookmarktitle, bookmark.bookmarkcode, bookmark.bookmarktrade, bookmark.bookmarkretail, bookmark.bookmarkdescription, bookmark.bookmarkimagepathS, bookmark.bookmarkimagepathM, bookmark.sectionid, bookmark.categoryid];
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            
            return DataBaseInsertionSuccessful;
        }
        
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    
    return DataBaseInsertionFailed;
}

-(DataBaseInsertionResult)insertMyjob:(Myjob*)myjob
{
    BOOL connectionIsOpenned=YES;
    
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    
    if(connectionIsOpenned)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into Myjob(Title, Date) values('%@', '%@');", myjob.myjobtitle, myjob.myjobdate];
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            
            return DataBaseInsertionSuccessful;
        }
        
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}

-(DataBaseInsertionResult)insertProduct:(Product*)product
{
    BOOL connectionIsOpenned=YES;
    
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    
    if(connectionIsOpenned)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into Product(Title, Code, Trade, Retail, Description, Quantity, ImagepathS, ImagepathM, Jobid, SectionID, CategoryID) values('%@', '%@', '%@', '%@', '%@', '%d', '%@', '%@','%d', '%d', '%d');", product.producttitle, product.productcode, product.producttrade, product.productretail, product.productdescription, product.productquantity, product.productimagepathS, product.productimagepathM, product.myjobid, product.sectionid, product.categoryid];
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            
            return DataBaseInsertionSuccessful;
        }
        
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}

-(NSArray*)retrieveBookmark
{
    NSMutableArray *itemarr = [[[NSMutableArray alloc] init] autorelease];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select Title, Code, Trade, Retail, Description, ImagepathS, ImagepathM, SectionID, CategoryID from Bookmark"];
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            Bookmark *bookmark= [[Bookmark alloc] init];
            
            char* titletext=(char*)sqlite3_column_text(statement, 0);
            
            if(titletext)
            {
                NSString *title=[NSString stringWithUTF8String:(char*)titletext];
                bookmark.bookmarktitle = title;
                //NSLog(@"%@", bookmark.bookmarktitle);
            }
            
            char* codetext=(char*)sqlite3_column_text(statement, 1);
            
            if(codetext)
            {
                NSString *code=[NSString stringWithUTF8String:(char*)codetext];
                bookmark.bookmarkcode = code;
                //NSLog(@"%@", bookmark.bookmarkcode);
            }
            
            char* tradetext=(char*)sqlite3_column_text(statement, 2);
            
            if(tradetext)
            {
                NSString *trade=[NSString stringWithUTF8String:(char*)tradetext];
                bookmark.bookmarktrade = trade;
                //NSLog(@"%@", bookmark.bookmarktrade);
            }
            
            char* retailtext=(char*)sqlite3_column_text(statement, 3);
            
            if(retailtext)
            {
                NSString *retail=[NSString stringWithUTF8String:(char*)retailtext];
                bookmark.bookmarkretail = retail;
                //NSLog(@"%@", bookmark.bookmarkretail);
            }
            
            char* descriptiontext=(char*)sqlite3_column_text(statement, 4);
            
            if(descriptiontext)
            {
                NSString *description=[NSString stringWithUTF8String:(char*)descriptiontext];
                bookmark.bookmarkdescription = description;
                //NSLog(@"%@", bookmark.bookmarkdescription);
            }
            
            char* imagepathS=(char*)sqlite3_column_text(statement, 5);
            
            if(imagepathS)
            {
                NSString *pathS=[NSString stringWithUTF8String:(char*)imagepathS];
                bookmark.bookmarkimagepathS = pathS;
                //NSLog(@"%@", bookmark.bookmarkimagepathS);
            }
            
            char* imagepathM=(char*)sqlite3_column_text(statement, 6);
            
            if(imagepathM)
            {
                NSString *pathM=[NSString stringWithUTF8String:(char*)imagepathM];
                bookmark.bookmarkimagepathM = pathM;
                //NSLog(@"%@", bookmark.bookmarkimagepathM);
            }
            
            int sectionid=sqlite3_column_int(statement, 7);
            bookmark.sectionid = sectionid;
            
            int categoryid=sqlite3_column_int(statement, 8);
            bookmark.categoryid = categoryid;

            [itemarr addObject:bookmark];
            [bookmark release];
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

-(NSArray*)retrieveMyjob
{
    NSMutableArray *itemarr = [[[NSMutableArray alloc] init] autorelease];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select Title, Date from Myjob order by MyjobID"];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            Myjob *myjob= [[Myjob alloc] init];
            
            char* titletext=(char*)sqlite3_column_text(statement, 0);
            
            if(titletext)
            {
                NSString *title=[NSString stringWithUTF8String:(char*)titletext];
                myjob.myjobtitle = title;
            }
            
            char* datetext=(char*)sqlite3_column_text(statement, 1);
            
            if(datetext)
            {
                NSString *date=[NSString stringWithUTF8String:(char*)datetext];
                myjob.myjobdate = date;
            }
            
            [itemarr addObject:myjob];
            [myjob release];
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

-(NSArray*)retrieveProduct:(int)myjobid
{
    NSMutableArray *itemarr = [[[NSMutableArray alloc] init] autorelease];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select Title, Code, Trade, Retail, Description, Quantity, ImagepathS, ImagepathM, Jobid, SectionID, CategoryID from Product where Jobid = %d", myjobid];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            Product *product= [[Product alloc] init];
            
            char* titletext=(char*)sqlite3_column_text(statement, 0);
            
            if(titletext)
            {
                NSString *title=[NSString stringWithUTF8String:(char*)titletext];
                product.producttitle = title;
            }
            
            char* codetext=(char*)sqlite3_column_text(statement, 1);
            
            if(codetext)
            {
                NSString *code=[NSString stringWithUTF8String:(char*)codetext];
                product.productcode = code;
            }
            
            char* tradetext=(char*)sqlite3_column_text(statement, 2);
            
            if(tradetext)
            {
                NSString *trade=[NSString stringWithUTF8String:(char*)tradetext];
                product.producttrade = trade;
            }
            
            char* retailtext=(char*)sqlite3_column_text(statement, 3);
            
            if(retailtext)
            {
                NSString *retail=[NSString stringWithUTF8String:(char*)retailtext];
                product.productretail = retail;
            }
            
            char* descriptiontext=(char*)sqlite3_column_text(statement, 4);
            
            if(descriptiontext)
            {
                NSString *description=[NSString stringWithUTF8String:(char*)descriptiontext];
                product.productdescription = description;
            }
            
            int quantity=sqlite3_column_int(statement, 5);
            product.productquantity = quantity;
            
            char* imagepathS=(char*)sqlite3_column_text(statement, 6);
            
            if(imagepathS)
            {
                NSString *pathS=[NSString stringWithUTF8String:(char*)imagepathS];
                product.productimagepathS = pathS;
            }
            
            char* imagepathM=(char*)sqlite3_column_text(statement, 7);
            
            if(imagepathM)
            {
                NSString *pathM=[NSString stringWithUTF8String:(char*)imagepathM];
                product.productimagepathM = pathM;
            }
            
            int jobid=sqlite3_column_int(statement, 8);
            product.myjobid = jobid;
            
            int sectionid=sqlite3_column_int(statement, 9);
            product.sectionid = sectionid;
            
            int categoryid=sqlite3_column_int(statement, 10);
            product.categoryid = categoryid;
            
            [itemarr addObject:product];
            [product release];
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

-(NSArray*)retrieveAllproduct
{
    NSMutableArray *itemarr = [[[NSMutableArray alloc] init] autorelease];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select Title, Code, Trade, Retail, Description, Quantity, ImagepathS, ImagepathM, Jobid, SectionID, CategoryID from Product;"];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            Product *product= [[Product alloc] init];
            
            char* titletext=(char*)sqlite3_column_text(statement, 0);
            
            if(titletext)
            {
                NSString *title=[NSString stringWithUTF8String:(char*)titletext];
                product.producttitle = title;
            }
            
            char* codetext=(char*)sqlite3_column_text(statement, 1);
            
            if(codetext)
            {
                NSString *code=[NSString stringWithUTF8String:(char*)codetext];
                product.productcode = code;
            }
            
            char* tradetext=(char*)sqlite3_column_text(statement, 2);
            
            if(tradetext)
            {
                NSString *trade=[NSString stringWithUTF8String:(char*)codetext];
                product.producttrade = trade;
            }
            
            char* retailtext=(char*)sqlite3_column_text(statement, 3);
            
            if(retailtext)
            {
                NSString *retail=[NSString stringWithUTF8String:(char*)codetext];
                product.productretail = retail;
            }
            
            char* descriptiontext=(char*)sqlite3_column_text(statement, 4);
            
            if(descriptiontext)
            {
                NSString *description=[NSString stringWithUTF8String:(char*)descriptiontext];
                product.productdescription = description;
            }
            
            int quantity=sqlite3_column_int(statement, 5);
            product.productquantity = quantity;
            
            char* imagepathS=(char*)sqlite3_column_text(statement, 6);
            
            if(imagepathS)
            {
                NSString *pathS=[NSString stringWithUTF8String:(char*)imagepathS];
                product.productimagepathS = pathS;
            }
            
            char* imagepathM=(char*)sqlite3_column_text(statement, 7);
            
            if(imagepathM)
            {
                NSString *pathM=[NSString stringWithUTF8String:(char*)imagepathM];
                product.productimagepathM = pathM;
            }
            
            int jobid=sqlite3_column_int(statement, 8);
            product.myjobid = jobid;
            
            int sectionid=sqlite3_column_int(statement, 9);
            product.sectionid = sectionid;
            
            int categoryid=sqlite3_column_int(statement, 10);
            product.categoryid = categoryid;
            
            [itemarr addObject:product];
            [product release];
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

-(NSString*)retrieveSelectedmyjobtitle:(int)myjobid
{
    NSString *title;
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select Title from Myjob where MyjobID = %d", myjobid];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            char* titletext=(char*)sqlite3_column_text(statement, 0);
            
            if(titletext)
            {
               title=[NSString stringWithUTF8String:(char*)titletext];
            }

        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return title;
}

-(int)retrieveBookmarkselectedrow:(int)row
{
    int lastid;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select BookmarkID from Bookmark order by BookmarkID limit 1 offset %d", row];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            lastid = sqlite3_column_int(statement, 0);

        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return lastid;
}

-(int)retrieveMyjobselectedrow:(int)row
{
    int lastid;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select MyjobID from Myjob order by MyjobID limit 1 offset %d", row];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            lastid = sqlite3_column_int(statement, 0);
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return lastid;
}

-(int)retrieveProductselectedrow:(int)row:(int)myjobid
{
    int lastid;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select ProductID from Product where Jobid = %d order by ProductID limit 1 offset %d", myjobid, row];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            lastid = sqlite3_column_int(statement, 0);
            
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return lastid;
}

-(int)countProductinjob:(int)myjobid:(NSString *)code
{
    int count = 0;
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select count(ProductID) from Product where Code = '%@' and Jobid = %d", code, myjobid];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            count=sqlite3_column_int(statement, 0);
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return count;
}

-(int)retrieveProductQuantity:(int)myjobid:(NSString *)code
{
    int count = 0;
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select Quantity from Product where Code = '%@' and Jobid = %d", code, myjobid];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            count=sqlite3_column_int(statement, 0);
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return count;
}

-(DataBaseUpdateResult)updateProductQuantity:(int)myjobid:(NSString *)code:(int)quantity
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString* updateSQL=[NSString stringWithFormat:@"update Product set Quantity=%d where Code = '%@' and Jobid = %d", quantity, code, myjobid];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateFailed;
    }
    else
    {
        return DataBaseUpdateFailed;
    }
}

-(DataBaseUpdateResult)updateMyjobtitle:(int)myjobid:(NSString *)title
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString* updateSQL=[NSString stringWithFormat:@"update Myjob set Title='%@' where MyjobID = %d", title, myjobid];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateFailed;
    }
    else
    {
        return DataBaseUpdateFailed;
    }
}

-(DataBaseDeletionResult)deleteAllbookmark
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"delete from Bookmark"];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        
    }
    
    return DataBaseDeletionFailed;
}

-(DataBaseDeletionResult)deleteBookmark:(int)bookmarkid
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"delete from Bookmark where BookmarkID = '%d'", bookmarkid];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        
    }
    
    return DataBaseDeletionFailed;
}

-(DataBaseDeletionResult)deleteAllmyjob
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"delete from Myjob"];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        
        [self closeConnection];
        
    }
    
    return DataBaseDeletionFailed;
}

-(DataBaseDeletionResult)deleteMyjob:(int)myjobid
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"delete from Myjob where MyjobID = %d", myjobid];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        
        [self closeConnection];
        
    }
    
    return DataBaseDeletionFailed;
}

-(DataBaseDeletionResult)deleteAllproduct
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"delete from Product"];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        
    }
    
    return DataBaseDeletionFailed;
}

-(DataBaseDeletionResult)deleteProduct:(int)productid:(int)myjobid
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"delete from Product where Productid = %d and Jobid = %d", productid, myjobid];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        
    }
    
    return DataBaseDeletionFailed;
}

-(DataBaseDeletionResult)deleteProductWithCode:(NSString*)code:(int)myjobid
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"delete from Product where Code = '%@' and Jobid = %d", code, myjobid];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        
    }
    return DataBaseDeletionFailed;
}

-(DataBaseDeletionResult)deleteProductWithJobid:(int)myjobid
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"delete from Product where Jobid = %d", myjobid];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        
    }
    
    return DataBaseDeletionFailed;
}

@end
